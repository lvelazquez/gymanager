# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ClientHistoryUI.ui'
#
# Created: Mon May 27 23:09:03 2013
#      by: PyQt4 UI code generator 4.10
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(780, 455)
        self.billhistory_table = QtGui.QTableWidget(Dialog)
        self.billhistory_table.setGeometry(QtCore.QRect(10, 40, 747, 301))
        self.billhistory_table.setObjectName(_fromUtf8("billhistory_table"))
        self.billhistory_table.setColumnCount(5)
        self.billhistory_table.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.billhistory_table.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.billhistory_table.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.billhistory_table.setHorizontalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.billhistory_table.setHorizontalHeaderItem(3, item)
        item = QtGui.QTableWidgetItem()
        self.billhistory_table.setHorizontalHeaderItem(4, item)
        self.billhistory_table.horizontalHeader().setDefaultSectionSize(149)
        self.billhistory_table.verticalHeader().setVisible(False)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Dialog", None))
        item = self.billhistory_table.horizontalHeaderItem(0)
        item.setText(_translate("Dialog", "Fecha", None))
        item = self.billhistory_table.horizontalHeaderItem(1)
        item.setText(_translate("Dialog", "Código", None))
        item = self.billhistory_table.horizontalHeaderItem(2)
        item.setText(_translate("Dialog", "Categoria", None))
        item = self.billhistory_table.horizontalHeaderItem(3)
        item.setText(_translate("Dialog", "Producto", None))
        item = self.billhistory_table.horizontalHeaderItem(4)
        item.setText(_translate("Dialog", "Precio", None))

