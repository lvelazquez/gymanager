#!/usr/bin/python
#coding=utf-8
 
from PyQt4 import QtGui, QtCore
from datetime import datetime,date

import sys
import Constants

 
# Import the interface class
import ClientFormUI


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

 
class ClientForm(QtGui.QDialog, ClientFormUI.Ui_Dialog):
    """ The second parent must be 'Ui_<obj. name of main widget class>'.
        If confusing, simply open up ImageViewer.py and get the class
        name used. I'd named mine as mainWindow, hence Ui_mainWindow. """
 
    def __init__(self, window_params=None, parent=None):
        super(ClientForm, self).__init__(parent)
        # This is because Python does not automatically
        # call the parent's constructor.
        self.setupUi(self)
        # Pass this "self" for building widgets and
        # keeping a reference.


        self.is_manager = window_params["is_manager"]
        client_info     = window_params["client_info"]
        self.is_new_client   = window_params['is_new_client'] if 'is_new_client' in window_params else None
        self.from_billing = True if 'from_billing' in window_params else False
        self.valid_code = False
        self.current_client = client_info
        self.set_handler()



        if not self.is_manager:
            #self.buttonBox.hide()
            if not self.from_billing:
                if not self.is_new_client:
                    client_info = window_params["client_info"]
                    self.render_client(client_info)
                    self.disable_form()
                    self.register_code_btn.clicked.connect(self.register_code)
                    self.is_edit= True
                else:
                    self.set_new_client_form()
                    self.is_edit = False

            else:
                self.set_from_billing_form()
                self.is_edit = False
        else:
            if client_info:
                self.render_client(client_info)
                self.set_edit_client()
                self.is_edit = True
            else:
                self.is_edit = False
                self.btn_bill_history.hide()

 
    def main(self):
        self.show()

    #Rewrite fuction accept
    def accept(self):


        if not self.from_billing and not self.is_new_client:
            valid_code = self.check_code()
            if not self.valid_code and not self.is_manager:
                return

        client_params = {}
        client_params["name"] = str(self.lineEdit_name.text())
        client_params["ci"]   = str(self.lineEdit_ci.text())
        client_params["address"] = str(self.lineEdit_address.text())
        client_params["tel_mobil"] = str(self.lineEdit_tlfm.text())
        client_params["tel_home"]  = str(self.lineEdit_tlfh.text())
        #signin_date  = datetime.strptime(str(self.dateEdit_sid.text()) , '%m/%d/%Y')
        signin_date  = date.today()
        client_params["signin_date"]  = signin_date
        client_params["photo"] = 'fotolevi'
        client_params["huellalevi"]='huellalevi'
        from controllers import ClientController
        new_client = ClientController.new_client(client_params)

        msg = 'Editado' if self.is_edit else 'Agregado'
        if new_client:

            if not self.from_billing and not self.is_new_client:
                registered_code = self.register_code()
                if not registered_code:
                    QtGui.QMessageBox.about(self,_fromUtf8("Notificación"),_fromUtf8("Cliente No"+msg))
                    return

            QtGui.QMessageBox.about(self,_fromUtf8("Notificación"),_fromUtf8("Cliente "+msg))
            self.done(Constants.OBJECT_CREATED)
        else:
            QtGui.QMessageBox.about(self,_fromUtf8("Notificación"),_fromUtf8("Cliente No"+msg))



    def render_client(self,user_info):
        #self.register_code_btn.clicked.connect(self.register_code)
        self.lineEdit_name.setText(user_info.name)
        self.lineEdit_ci.setText(str(user_info.ci))
        self.lineEdit_tlfm.setText(str(user_info.tel_mobil))
        self.lineEdit_tlfh.setText(str(user_info.tel_home))
        self.lineEdit_address.setText(str(user_info.address))
        self.lineEdit_membership.setText(str(user_info.membership))
        #_date_sid = user_info.signin_date
        _date_sod = user_info.signout_date
        #date_sid  = QtCore.QDate(_date_sid.year,_date_sid.month,_date_sid.day)
        date_sod  = QtCore.QDate(_date_sod.year,_date_sod.month,_date_sod.day)
        #self.dateEdit_sid.setDate(date_sid)
        self.dateEdit_sod.setDate(date_sod)

    def disable_form(self):
        self.lineEdit_name.setReadOnly(True)
        self.lineEdit_ci.setReadOnly(True)
        self.lineEdit_tlfm.setReadOnly(True)
        self.lineEdit_tlfh.setReadOnly(True)
        self.lineEdit_address.setReadOnly(True)
        self.dateEdit_sod.setReadOnly(True)
        self.dateEdit_sod.setReadOnly(True)
        self.lineEdit_membership.setReadOnly(True)
        self.buttonBox.hide()
        #self.btn_bill_history.hide()


    def set_new_client_form(self):

        self.label_membership.hide()
        self.label_sod.hide()
        self.lineEdit_membership.hide()
        self.dateEdit_sod.hide()
        self.register_code_btn.hide()
        self.lineEdit_billing_code.hide()
        self.label_code.hide()
        self.btn_bill_history.hide()

    def set_from_billing_form(self):
        _date_sod = date.today()
        date_sod  = QtCore.QDate(_date_sod.year,_date_sod.month,_date_sod.day)
        self.dateEdit_sod.setDate(date_sod)
        self.label_code.hide()
        self.label_membership.hide()
        self.lineEdit_billing_code.hide()
        self.register_code_btn.hide()
        self.lineEdit_membership.hide()

    def set_edit_client(self):
        self.lineEdit_ci.setReadOnly(True)

    def register_code(self):
        ci   = str(self.lineEdit_ci.text())
        code = str(self.lineEdit_billing_code.text())
        if not len(code):
            return
        from controllers import ClientController
        try:
            is_registered   =  ClientController.register_code(ci,code)
            self.valid_code = True
            QtGui.QMessageBox.about(self,_fromUtf8("Notificación"),_fromUtf8("Código Registrado"))
            return True
        except Exception,e:
            QtGui.QMessageBox.about(self,_fromUtf8("Notificación"),_fromUtf8(str(e)))
            return False

    def check_code(self):

        code_type = None
        if self.is_edit:
            code_type = 'monthly_payment_code'
        else:
            code_type = 'enrollment'
        code = str(self.lineEdit_billing_code.text())
        if not code and not self.is_manager:
            QtGui.QMessageBox.about(self,_fromUtf8("Notificación"),_fromUtf8("Necesita insertar un código"))
            return False
        from controllers import ClientController
        try:
            is_valid   =  ClientController.code_is_valid(code,code_type)
            self.valid_code = is_valid
            QtGui.QMessageBox.about(self,_fromUtf8("Notificación"),_fromUtf8("Código Registrado"))
            return True
        except Exception,e:
            QtGui.QMessageBox.about(self,_fromUtf8("Notificación"),_fromUtf8(str(e)))
            return False


    def open_bill_history(self):
        from ClientHistory import ClientHistory 
        window_params = {}
        window_params["client"]  = self.current_client
        clientHistory = ClientHistory(window_params)
        clientHistory.exec_()


    def set_handler(self):
        self.btn_bill_history.clicked.connect(self.open_bill_history)

if __name__=='__main__':
    app = QtGui.QApplication(sys.argv)
    ClientForm = ClientForm()
    ClientForm.main()
    app.exec_()
    # This shows the interface we just created. No logic has been added, yet.