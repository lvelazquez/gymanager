#!/usr/bin/python
#coding=utf-8
 
from PyQt4 import QtGui, QtCore
from Utils import *
from datetime import datetime,date
import Constants
import sys



try:
	_fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
	def _fromUtf8(s):
		return s

 
# Import the interface class
import ProductsReportUI


 
class ProductsReport(QtGui.QDialog, ProductsReportUI.Ui_Dialog):
	""" The second parent must be 'Ui_<obj. name of main widget class>'.
		If confusing, simply open up ImageViewer.py and get the class
		name used. I'd named mine as mainWindow, hence Ui_mainWindow. """

 
	def __init__(self, params=None,parent=None):
		super(ProductsReport,self).__init__(parent)
		# This is because Python does not automatically
		# call the parent's constructor.
		self.setupUi(self)


		self.sales_1 = None
		self.sales_2 = None
		#Settings click handlers

		#self.productTable.clicked.connect(self.select_product)
		#self.lineEdit_search.returnPressed.connect(self.search_product)
		params = {}
		params['user'] = Constants.MANAGER
		self.is_manager = True if params['user'] == Constants.MANAGER else False

		#if self.is_manager:
		#    self.new_product_btn.clicked.connect(self.new_product)
		#else:
		#    self.new_product_btn.hide()

		#set client list
		self.set_sales_table()
		self.set_hanlders()
		#from controllers import SpendingController
		#SpendingController.total_price(ss)
		#SpendingController.today_spendings()

		# Pass this "self" for building widgets and
		# keeping a reference.

 
	def main(self):
		self.show()

	def select_sale(self,itemclicked,table):

		# #selected row index
		row = itemclicked.row()
		max_column = table.columnCount() - 1
		range_selected = QtGui.QTableWidgetSelectionRange(row,0,row,max_column)
		table.setRangeSelected(range_selected,True)

		sale_code = str(table.item(row,0).text())
		return sale_code


	#set initial client table configuration
	def set_sales_table(self):

		#render client list
		self.render_sales_list()
		today = date.today()
		self.dateEdit_search_month_1.setDate(QtCore.QDate(today.year, today.month,today.day))
		self.dateEdit_search_year_1.setDate(QtCore.QDate(today.year, today.month,today.day))


		#set no editable
		#if not self.is_manager:
		self.sales_table_cashier1.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
		self.sales_table_cashier2.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)

	def set_hanlders(self):
		#self.lineEdit_search.returnPressed.connect(self.search_spending)
		self.dateEdit_search_month_1.\
		dateChanged.connect(self.search_by_date)
		self.dateEdit_search_year_1.\
		dateChanged.connect(self.search_by_date)
		#self.sales_table_cashier1.doubleClicked.connect(self.view_selected_sale)
		cashier1_table = self.sales_table_cashier1
		cashier2_table = self.sales_table_cashier2
		self.sales_table_cashier1.clicked.connect(lambda item,table=cashier1_table:\
												  self.select_sale(item,table)
												 )
		self.sales_table_cashier2.clicked.connect(lambda item,table=cashier2_table:\
												  self.select_sale(item,table)
												 )


	#render complete client list
	def render_sales_list(self):
		from controllers import ProductController
		today = date.today()
		cashier_1 = 1
		cashier_2 = 2
		sales_1 = ProductController.products_report(today.month,today.year,cashier_1) if not self.sales_1 else self.sales_1
		self.sales_1 = sales_1
		items = []
		for sale in sales_1:
			items.append(Utils.set_att(sale))

		atts_to_render = ['product','amount','total']

		Utils.render_items_list(self.sales_table_cashier1,items,atts_to_render)

		sales_2 = ProductController.products_report(today.month,today.year,cashier_1) if not self.sales_2 else self.sales_2
		self.sales_2 = sales_2
		items = []
		for sale in sales_2:
			items.append(Utils.set_att(sale))

		atts_to_render = ['product','amount','total']

		Utils.render_items_list(self.sales_table_cashier2,items,atts_to_render)


		#total = sum(map(lambda x:x['price'],sales_1))
		#self.total_sales.display(total)

	def render_matched_sales(self,sales_items):
		Utils.render_matched_items(self.sales_table,sales_items)


	#Handler for new_client_btn click
	def new_product(self):
		result = self.open_product_window()
		self.render_product_list()

	def search_spending(self):

		search_string = self.lineEdit_search.text()
		#if search string is empty, render complete client table
		self.render_sales_list()            
		if not search_string:
			return

		matched_spendings = self.sales_table.findItems(search_string, QtCore.Qt.MatchContains)

		if matched_spendings:
			self.render_matched_sales(matched_spendings)
		
	def search_by_date(self):
		from controllers import ProductController
		month  = datetime.strptime(str(self.dateEdit_search_month_1.text()) , '%m').month
		year   = datetime.strptime(str(self.dateEdit_search_year_1.text()) , "%Y").year
		cashier_1 = 1
		cashier_2 = 2
		sales = ProductController.products_report(month,year,cashier_1)
		items = []
		for sale in sales:
			items.append(Utils.set_att(sale))

		atts_to_render = ['product','amount','total']
		
		Utils.render_items_list(self.sales_table_cashier1,items,atts_to_render)

		sales = ProductController.products_report(month,year,cashier_2)
		items = []
		for sale in sales:
			items.append(Utils.set_att(sale))

		atts_to_render = ['product','amount','total']
		
		Utils.render_items_list(self.sales_table_cashier2,items,atts_to_render)
