#!/usr/bin/python
#coding=utf-8
 
from PyQt4 import QtGui, QtCore

import sys
 
# Import the interface class
import SettingsUI


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

 
class Settings(QtGui.QDialog, SettingsUI.Ui_Dialog):
    """ The second parent must be 'Ui_<obj. name of main widget class>'.
        If confusing, simply open up ImageViewer.py and get the class
        name used. I'd named mine as mainWindow, hence Ui_mainWindow. """
 
    def __init__(self, window_params=None, parent=None):
        super(Settings, self).__init__(parent)
        # This is because Python does not automatically
        # call the parent's constructor.
        self.setupUi(self)
        # Pass this "self" for building widgets and
        # keeping a reference.
        from controllers import SettingsController

        current_iva = SettingsController.get_iva()
        self.lineEdit_IVA.setText(_fromUtf8(str(current_iva)))
        self.is_manager = False
 
    def main(self):
        self.show()

    #Rewrite fuction accept
    def accept(self):
        from controllers import SettingsController
        accept_msg = 'Desea cambiar el IVA.'
        reply = QtGui.QMessageBox.question(self, 'Message', 
                         accept_msg, QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)

        if reply == QtGui.QMessageBox.Yes:
            new_iva = self.lineEdit_IVA.text();
            SettingsController.change_iva(new_iva)
    

if __name__=='__main__':
    app = QtGui.QApplication(sys.argv)
    Settings = Settings()
    Settings.main()
    app.exec_()
