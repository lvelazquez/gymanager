#coding=utf-8
import sqlalchemy
from sqlalchemy import create_engine,extract,func
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Date,ForeignKey,Float,Table
from sqlalchemy.orm import relationship, backref
from sqlalchemy.orm import sessionmaker,exc
from sqlalchemy import Date, cast
from datetime import datetime,date,timedelta

engine = create_engine('sqlite:///database_gym.db', echo=False,encoding="utf-8")

Session = sessionmaker(bind=engine)
session = Session()


print "sdad",engine.execute("select 1").scalar()


Base = declarative_base()



class User(Base):
	__tablename__='user'

	username    = Column(String(50),primary_key=True)
	password    = Column(String(50))
	role        = Column(String(50))


class Settings(Base):
	__tablename__='settings'

	iva         = Column(Integer,primary_key=True)
	id          = Column(Integer)

	def __init__(self,iva):
		self.iva = iva

	@classmethod
	def create(self,iva):
		settings = Settings(iva)
		session.add(settings)
		session.commit()

	@classmethod
	def edit_iva(self,new_iva):
		settings = session.query(Settings).first()
		settings.iva = int(new_iva)
		session.commit()
		
	@classmethod
	def get_iva(self):
		settings = session.query(Settings).first()
		return	settings.iva



class OwnerLocker(Base):
	__tablename__='ownerlocker'

	client_id = Column(Integer, ForeignKey('client.ci'), primary_key=True)
	locker_id = Column(Integer, ForeignKey('locker.code'), primary_key=True)
	status    = Column(String(10))
	signin_date     = Column(Date)
	expiration_date = Column(Date)
	client = relationship("Client", backref="assigned_locker")


class Locker(Base):
	__tablename__ = 'locker'

	code   = Column(Integer,primary_key=True)
	owners = relationship("OwnerLocker",backref="locker")

	def __init__(self, code):
		self.code = code		


#Falta colocar la referencia al Locker
class Client(Base):
	__tablename__ = 'client'

	name = Column(String(100))
	ci       = Column(Integer,primary_key=True)
	address  = Column(String(100))
	tel_mobil    = Column(Integer)
	tel_home     = Column(Integer)
	signin_date  = Column(Date)
	signout_date = Column(Date)
	photo        = Column(String(100))
	figerprint   = Column(String(100))
	status       = Column(String(100))
	membership   = Column(String(100))


	def __init__(self,**kwargs):

		for key in kwargs.keys():
			val = kwargs[key]
			exec("self.%s = val" % key)



	@classmethod
	def add_client(cls,client_params):
		client = session.query(cls).get(client_params["ci"])

		if not client:
			client = Client(**client_params)
			session.add(client)
		else:
			client.update(client_params)
		
		session.commit()
		return client


	#Búsqueda del cliente por cédula
	@classmethod
	def get_by_ci(cls,ci):
		user = session.query(cls).get(ci)
		return user

	#Búsqueda del cliente por nombre
	@classmethod
	def get_by_name(cls,name):
		user = session.query(cls).filter(cls.name==name)
		return user

	@classmethod
	def all(cls):
		clients = session.query(cls).all()
		return clients

	def update(self,kwargs):
		for key in kwargs.keys():
			val = kwargs[key]
			exec("self.%s = val" % key)

	def set_membership(self,new_membership):
		self.membership = new_membership
		session.flush()

	def set_new_sod(self,new_sod):
		self.signout_date = new_sod
		session.flush()

	def sales_history(self):

		today = date.today()
		DD = timedelta(days=365)
		earlier = today - DD
		earlier_str = earlier.strftime("%Y%m%d")

		return self.sales


class Product(Base):
	__tablename__ = 'product'

	name   = Column(String(100))
	id   = Column(Integer,primary_key=True)
	price  = Column(Integer)
	amount = Column(Integer)
	category = Column(String(100))


	def __init__(self,**kwargs):

		for key in kwargs.keys():
			val = kwargs[key]
			exec("self.%s = val" % key)

	def __repr__(self):
		return '<%s / >' % (
				self.name
				)


	@classmethod
	def get_by_code(cls,code):
		product = session.query(cls).get(code)
		return product

	@classmethod
	def all(cls):
		products = session.query(cls).all()
		return products


	@classmethod
	def add(cls,product_params):
		product = None
		if 'id' in product_params:
			product = session.query(cls).get(product_params["id"])
		if not product:
			product = Product(**product_params)
			session.add(product)
		else:
			product.update(product_params)

		
		session.commit()
		return product

	@classmethod
	def delete(cls,product_id):
		try:
			product = session.query(cls).get(product_id)
			session.delete(product)
		except Exception,e:
			raise Exception(e)

		session.commit()

	def update(self,kwargs):
		for key in kwargs.keys():
			val = kwargs[key]
			exec("self.%s = val" % key)

	@classmethod
	def get_by_category(cls,category):
		products = session.query(cls).filter(cls.category==str(category)).all()
		return products

	@classmethod
	def products_report(cls,month,year,cashier_number):
		ps = session.query(Product.name.label('product'),\
				   func.count(Product.category).label('amount'),\
				   func.sum(ProductSold.price_sold).label('price'),\
				   extract('day',Sales.created_date).label('day'))\
							   .outerjoin(ProductSold,ProductSold.product_id==Product.id)\
							   .filter(Sales.id==ProductSold.sales_id)\
							   .filter(Sales.cashier_number==cashier_number)\
							   .filter(extract('month',Sales.created_date)==month)\
							   .filter(extract('year',Sales.created_date)==year)\
							   .group_by(Product.name)\
							   .subquery()

		products = session.query(Product.name.label('product'),\
										ps.c.amount.label('amount'),\
										ps.c.price.label('total'))\
										.outerjoin(ps,Product.name==ps.c.product)\
										.all()

		return products

class ProductSold(Base):
	__tablename__ = 'product_sold'


	id  = Column(Integer,primary_key=True)
	product_id = Column(Integer, ForeignKey('product.id'))
	product     = relationship("Product",backref='solds')
	#product_id = Column(Integer, ForeignKey('product.id')primary_key=True)
	sales_id   = Column(Integer, ForeignKey('sales.id'))
	price_sold  = Column(Integer)


	def __init__(self,**kwargs):

		for key in kwargs.keys():
			val = kwargs[key]
			exec("self.%s = val" % key)

	def __repr__(self):
		return '<%s / price(%s) / sale:(%s)>' % (
				self.product.name,self.price_sold,self.sale.client.name
				)

	def to_dict(self):
		return {
			'category':self.product.category,
			'name':self.product.name,
			'price':self.price_sold
		}


class Spending(Base):
	__tablename__ = 'spending'


	beneficiary   = Column(String(100))
	ci_rif        = Column(Integer)
	id            = Column(Integer,primary_key=True)
	concept       = Column(String(100))
	created_date  = Column(Date)
	payments  = relationship("Payment", backref="payment")
	total     = Column(Integer)
	cashier_number = Column(Integer)


	def __init__(self,**kwargs):
		for key in kwargs.keys():
			val = kwargs[key]
			exec("self.%s = val" % key)

	def __repr__(self):
		return '<%s %s (%s) (%s)| pagos: %s>' % (
				self.__class__.__name__, self.id, self.concept,
				self.created_date,self.payments
				)

	@classmethod
	def add(cls,spending_params,payments):
		new_spending = Spending(**spending_params)
		session.add(new_spending)
		session.flush()
		total = 0
		for pay_params in payments:
			pay_params['spending'] = new_spending.id
			total  += int(pay_params['amount'])
			payment = Payment.add(pay_params)

		new_spending.total = total

		#else:
		#	spending.update(spending_params)

		session.commit()		
		return new_spending

	@classmethod
	def all(cls):
		spendings = session.query(cls).all()
		return spendings


	@classmethod
	def get_by_date(self,date,cashier_number):
		_date = date.strftime('%Y-%m-%d')
		spendings = session.query(Spending).\
		filter(Spending.created_date==_date).\
		filter(Spending.cashier_number==cashier_number)\
		.all()

		return spendings

	@classmethod
	def get_by_month_year(self,month,year,cashier_number):
		spendings = session.query(Spending)\
		.filter(extract('month',Spending.created_date)==month,\
		        extract('year',Spending.created_date)==year)\
		.filter(Spending.cashier_number==cashier_number)\
		.all()
		return spendings




class Payment(Base):
	__tablename__ = 'payment'
	#__table_args__ = {'sqlite_autoincrement': True}

	type      = Column(String(100))
	bank      = Column(String(100))
	amount    = Column(Float)
	confirmation_number = Column(String(100))
	id       = Column(Integer,primary_key=True)
	spending = Column(Integer, ForeignKey('spending.id'))
	sales    = Column(Integer, ForeignKey('sales.id'))


	def __init__(self,**kwargs):

		for key in kwargs.keys():
			val = kwargs[key]
			exec("self.%s = val" % key)

	def __repr__(self):
		return '<%s %s-%s / >' % (
				self.__class__.__name__, self.type, self.amount
				)

	def to_dict(self):
		return {
			'amount':self.amount,
			'bank':self.bank,
			'confirmation_number':self.confirmation_number if self.confirmation_number else ''
		}

	@classmethod
	def add(cls,payment_params):

		payment = Payment(**payment_params)
		session.add(payment)
		session.commit()
		return payment


class Sales(Base):
	__tablename__ = 'sales'

	products_sold  = relationship("ProductSold",backref='sale')
	client_id = Column(Integer, ForeignKey('client.ci'))
	client 	  = relationship("Client", backref="sales")
	payments  = relationship("Payment", backref="sales_payment")
	id        = Column(Integer,primary_key=True)
	created_date     = Column(Date,default=date.today)
	total     = Column(Integer)
	cashier_number = Column(Integer)

	def __init__(self,**kwargs):
		for key in kwargs.keys():
			val = kwargs[key]
			exec("self.%s = val" % key)

	# def __repr__(self):
	# 	return '<%s %s (%s) (%s)| forma de pago: %s>' % (
	# 			self.__class__.__name__, self.id, self.products_sold,
	# 			self.created_date,self.payments
	# 			)

	def to_dict(self):
		return{
			'client_name':self.client.name,
			'date':self.created_date,
			'id':self.id,
			'payments': [p.to_dict() for p in self.payments],
			'total':self.total,
			'products_sold':[p.to_dict() for p in self.products_sold],
		}

	@classmethod
	def add(cls,sale_params,payments,products,client):
		try:
			new_sale = Sales(**sale_params)
			session.add(new_sale)
			session.flush()
			total = 0
			for pay_params in payments:
				pay_params['sales'] = new_sale.id
				total  += int(pay_params['amount'])
				payment = Payment.add(pay_params)


			products_available = Product.all()

			for product_params in products:
				price = product_params['price']
				product_general = product_params['product_general']
				proudct_general_id = product_general.id
				product_sold = ProductSold(price_sold=price,product_id=proudct_general_id,sales_id=new_sale.id)
				session.add(product_sold)
				session.flush()

			#new_sale.client.id = client.id
			#session.flush()
			new_sale.client = client

			new_sale.total = total
			session.commit()
			return new_sale
		except Exception,e:
			session.rollback()
			return False


	@classmethod
	def delete(cls,code):
		try:
			sale = session.query(cls).get(code)
			session.delete(sale)
			session.commit()
		except Exception,e:
			session.rollback()

	@classmethod
	def all(cls):
		sales = session.query(cls).all()
		return sales


	@classmethod
	def get_by_date(self,date,cashier_number):
		_date = date.strftime('%Y-%m-%d')
		sales = session.query(Sales).\
		filter(Sales.created_date==_date).\
		filter(Sales.cashier_number==cashier_number).\
		all()
		return sales

	@classmethod
	def get_by_month_year(self,month,year,cashier_number):
		sales = session.query(
				   func.count(Product.name).label('amount'),\
				   func.sum(ProductSold.price_sold).label('price'),\
				   extract('day',Sales.created_date).label('day'))\
							   .filter(ProductSold.product_id==Product.id)\
							   .filter(Sales.id==ProductSold.sales_id)\
							   .filter(Sales.cashier_number==cashier_number)\
							   .filter(extract('month',Sales.created_date)==month)\
							   .filter(extract('year',Sales.created_date)==year)\
							   .group_by('day')\
							   .all()



		return sales



	@classmethod
	def get_by_client(self,client_ci):
		sales = session.query(Sales).filter(Sales.clients.any(ci=client_ci)).all()
		return sales

	@classmethod
	def get_by_client_lastYear(self,client_ci):
		today = date.today()
		DD = timedelta(days=365)
		earlier = today - DD
		earlier_str = earlier.strftime("%Y-%m-%d")
		sales = session.query(Sales).filter(Sales.client_id==client_ci)\
				.filter(Sales.created_date>=earlier_str).all()
		return sales



code_clients_table = Table('association_code_clients', Base.metadata,
	Column('client_id', Integer, ForeignKey('client.ci')),
	Column('code_id', Integer, ForeignKey('billingmonthlycode.code'))
)


class BillingMonthlyCode(Base):
	__tablename__ = 'billingmonthlycode'

	code   = Column(String(100),primary_key=True)
	signout_date = Column(Date)
	valid_for    = Column(Integer)
	clients   = relationship("Client",secondary=code_clients_table)
	type      = Column(String(100))


	def __init__(self,**kwargs):

		for key in kwargs.keys():
			val = kwargs[key]
			exec("self.%s = val" % key)

	@classmethod
	def add(self,billing_code_params):
		new_code = BillingMonthlyCode(**billing_code_params)
		session.add(new_code)
		session.flush()
		return new_code

	@classmethod
	def get_by_code(cls,code):
		code = session.query(cls).get(code)
		return code

	def use_code(self,client):
		if not self.valid_for:
			raise Exception("Codigo usado completamente")
		self.valid_for -= 1
		self.clients.append(client)
		session.commit()
		return True

#for x in session.query(BillingMonthlyCode).all():
#	print x.code,x.type,x.valid_for
#print session.query(BillingMonthlyCode).all()[::-1][0].code

# session.commit()
#ci = 19737450

#today = date.today()
#DD = timedelta(days=365)
#earlier = today - DD
#earlier_str = earlier.strftime("%Y-%m-%d")

#print earlier_str
#_new_sod = datetime.strptime(str(today.year)+str(today.month)+str(today.day), "%Y%m%d").date()

# month=6
# year=2013

# ps = session.query(
# 				   func.count(Product.name).label('amount'),\
# 				   func.sum(ProductSold.price_sold).label('price'),\
# 				   extract('day',Sales.created_date).label('day'))\
# 							   .filter(ProductSold.product_id==Product.id)\
# 							   .filter(Sales.id==ProductSold.sales_id)\
# 							   .filter(extract('month',Sales.created_date)==month)\
# 							   .filter(extract('year',Sales.created_date)==year)\
# 							   .group_by('day')\
# 							   .all()



# ps = session.query(Product.name,ps.c.amount,ps.c.price)\
# 								.outerjoin(ps,Product.name==ps.c.product)\
# 								.all()



us = session.query(Spending).all()
for u in us:
	print "asdasdas",u.cashier_number

#date = date.today()
#product = Product.all()
# for p in ps:
# 	print p

# print "#############################"
# for p in ps:
# 	print p

# print "#############################"



#ps = Sales.get_by_month_year(month,year)
#for p in ps:
#	print p.price

#print Sales.created_date.__dict__

#sales = session.query(Sales).filter(Sales.client_id==19737450).filter(Sales.created_date>=earlier_str).all()
#levi = session.query(Client).filter(Client.sales.any(Sales.created_date>=earlier_str)) \
#.filter(Client.ci==19737450).one()
#qry = levi.filter(Sales.created_date<='2014-01-02')
#print sales
# print "LEVI",levi.ci
#sales = levi.sales
# for sale in sales:
# 	print sale
#print session.query(Sales).all()
#sales = session.query(Sales).filter(Sales.clients.any(ci=ci)).all()
# sales_2 = session.query(Sales).filter(Sales.clients.any(name='alberto')).all()
# sales_3 = session.query(Sales).filter(Sales.clients.any()).all()
#print sales
#for x in session.query(ProductSold).all():
#	print x
#print session.query(ProductSold).all()[0]

# Base.metadata.drop_all(engine)
# Base.metadata.create_all(engine)
# session.add(Product(**{'name':'Anual','price':3600,'amount':1,'category':'Mensualidad'}))
# session.add(Product(**{'name':'Semestral','price':2100,'amount':1,'category':'Mensualidad'}))
# session.add(Product(**{'name':'Trimestral','price':1100,'amount':1,'category':'Mensualidad'}))
# session.add(Product(**{'name':'Mensualidad','price':460,'amount':1,'category':'Mensualidad'}))
# session.add(Product(**{'name':'1-5','price':420,'amount':1,'category':'Mensualidad'}))

# session.add(Product(**{'name':'1 Persona','price':420,'amount':1,'category':'Inscripcion'}))
# session.add(Product(**{'name':'2 Personas','price':750,'amount':1,'category':'Inscripcion'}))
# session.add(Product(**{'name':'3 Personas','price':1060,'amount':1,'category':'Inscripcion'}))
# session.add(Product(**{'name':'4 Personas','price':1350,'amount':1,'category':'Inscripcion'}))
# session.add(Product(**{'name':'5 Personas','price':1550,'amount':1,'category':'Inscripcion'}))

# session.add(Product(**{'name':'Mensualidad sin Inscripcion','price':540,'amount':1,'category':'Planes'}))
# session.add(Product(**{'name':'Diario','price':70,'amount':1,'category':'Planes'}))

# session.add(Product(**{'name':'3-6 Meses','price':210,'amount':1,'category':'Reinscripcion'}))
# session.add(Product(**{'name':'6-12 Meses','price':230,'amount':1,'category':'Reinscripcion'}))
# session.add(Product(**{'name':'12+ Meses','price':250,'amount':1,'category':'Reinscripcion'}))

# session.add(Product(**{'name':'Mantenimiento','price':250,'amount':1,'category':'Mantenimiento'}))


# session.add(Product(**{'name':'Pepsi','price':20,'amount':22,'category':'Bebida'}))
# session.add(Product(**{'name':'Locker','price':20,'amount':1,'category':'Locker'}))
# session.add(Product(**{'name':'TRX','price':20,'amount':1,'category':'Reservacion'}))
# session.add(Product(**{'name':'Ciclismo','price':20,'amount':1,'category':'Reservacion'}))

# session.commit()


#spe = Spending.all()
#for s in spe:
#	print s


# print Client.__table__

# levi = Client('Levi','19737450','direccion','4','4','2007-08-05','2007-12-05','fotolevi','huellalevi')
# katy = Client('Katy','19000000','direccion','4','4','2007-08-05','2007-12-05','fotokaty','huellakaty')
# locker_1 = Locker('fg2s')

# owner = OwnerLocker(status='attached',signin_date='2007-08-05',expiration_date='2007-08-05')
# owner.client = levi
# print locker_1
# print locker_1.owners.append(owner)

# print levi.name
# for owner in locker_1.owners:
# 	print owner.client
