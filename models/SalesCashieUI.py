# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'SalesCashierUI.ui'
#
# Created: Thu Jun 20 23:54:22 2013
#      by: PyQt4 UI code generator 4.10
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(793, 574)
        self.label = QtGui.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(30, 140, 131, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayoutWidget_3 = QtGui.QWidget(Dialog)
        self.horizontalLayoutWidget_3.setGeometry(QtCore.QRect(40, 480, 162, 31))
        self.horizontalLayoutWidget_3.setObjectName(_fromUtf8("horizontalLayoutWidget_3"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout(self.horizontalLayoutWidget_3)
        self.horizontalLayout_3.setMargin(0)
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.label_4 = QtGui.QLabel(self.horizontalLayoutWidget_3)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.horizontalLayout_3.addWidget(self.label_4)
        self.dateEdit_search = QtGui.QDateEdit(self.horizontalLayoutWidget_3)
        self.dateEdit_search.setDate(QtCore.QDate(2013, 1, 31))
        self.dateEdit_search.setCalendarPopup(True)
        self.dateEdit_search.setObjectName(_fromUtf8("dateEdit_search"))
        self.horizontalLayout_3.addWidget(self.dateEdit_search)
        self.horizontalLayoutWidget = QtGui.QWidget(Dialog)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(440, 130, 241, 31))
        self.horizontalLayoutWidget.setObjectName(_fromUtf8("horizontalLayoutWidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label_2 = QtGui.QLabel(self.horizontalLayoutWidget)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout.addWidget(self.label_2)
        self.lineEdit_search = QtGui.QLineEdit(self.horizontalLayoutWidget)
        self.lineEdit_search.setObjectName(_fromUtf8("lineEdit_search"))
        self.horizontalLayout.addWidget(self.lineEdit_search)
        self.line = QtGui.QFrame(Dialog)
        self.line.setGeometry(QtCore.QRect(30, 110, 651, 20))
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.horizontalLayoutWidget_2 = QtGui.QWidget(Dialog)
        self.horizontalLayoutWidget_2.setGeometry(QtCore.QRect(520, 480, 160, 41))
        self.horizontalLayoutWidget_2.setObjectName(_fromUtf8("horizontalLayoutWidget_2"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.horizontalLayoutWidget_2)
        self.horizontalLayout_2.setMargin(0)
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label_3 = QtGui.QLabel(self.horizontalLayoutWidget_2)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_2.addWidget(self.label_3)
        self.total_sales = QtGui.QLCDNumber(self.horizontalLayoutWidget_2)
        self.total_sales.setObjectName(_fromUtf8("total_sales"))
        self.horizontalLayout_2.addWidget(self.total_sales)
        self.sales_table = QtGui.QTableWidget(Dialog)
        self.sales_table.setGeometry(QtCore.QRect(30, 170, 747, 301))
        self.sales_table.setObjectName(_fromUtf8("sales_table"))
        self.sales_table.setColumnCount(6)
        self.sales_table.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.sales_table.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.sales_table.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.sales_table.setHorizontalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.sales_table.setHorizontalHeaderItem(3, item)
        item = QtGui.QTableWidgetItem()
        self.sales_table.setHorizontalHeaderItem(4, item)
        item = QtGui.QTableWidgetItem()
        self.sales_table.setHorizontalHeaderItem(5, item)
        self.sales_table.horizontalHeader().setDefaultSectionSize(149)
        self.sales_table.verticalHeader().setVisible(False)
        self.delete_sale_btn = QtGui.QPushButton(Dialog)
        self.delete_sale_btn.setEnabled(False)
        self.delete_sale_btn.setGeometry(QtCore.QRect(310, 480, 131, 31))
        self.delete_sale_btn.setFocusPolicy(QtCore.Qt.NoFocus)
        self.delete_sale_btn.setObjectName(_fromUtf8("delete_sale_btn"))

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Dialog", None))
        self.label.setText(_translate("Dialog", "Ventas Diarias", None))
        self.label_4.setText(_translate("Dialog", "Fecha", None))
        self.dateEdit_search.setDisplayFormat(_translate("Dialog", "M/d/yyyy", None))
        self.label_2.setText(_translate("Dialog", "Búsqueda", None))
        self.label_3.setText(_translate("Dialog", "Total", None))
        item = self.sales_table.horizontalHeaderItem(0)
        item.setText(_translate("Dialog", "Fecha", None))
        item = self.sales_table.horizontalHeaderItem(1)
        item.setText(_translate("Dialog", "Código", None))
        item = self.sales_table.horizontalHeaderItem(2)
        item.setText(_translate("Dialog", "Cliente", None))
        item = self.sales_table.horizontalHeaderItem(3)
        item.setText(_translate("Dialog", "Categoria", None))
        item = self.sales_table.horizontalHeaderItem(4)
        item.setText(_translate("Dialog", "Producto", None))
        item = self.sales_table.horizontalHeaderItem(5)
        item.setText(_translate("Dialog", "Precio", None))
        self.delete_sale_btn.setText(_translate("Dialog", "Nota de Credito", None))

