#!/usr/bin/python
#coding=utf-8
 
from PyQt4 import QtGui, QtCore
from Utils import *

import sys
 
# Import the interface class
import SpendingFormUI
from datetime import datetime,date


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

 
class SpendingForm(QtGui.QDialog, SpendingFormUI.Ui_Dialog):
    """ The second parent must be 'Ui_<obj. name of main widget class>'.
        If confusing, simply open up ImageViewer.py and get the class
        name used. I'd named mine as mainWindow, hence Ui_mainWindow. """
 
    def __init__(self, window_params=None, parent=None):
        super(SpendingForm, self).__init__(parent)
        # This is because Python does not automatically
        # call the parent's constructor.
        self.setupUi(self)
        # Pass this "self" for building widgets and
        # keeping a reference.
        #from controllers import SettingsController
        self.cashier_number = window_params['cashier_number']
        self.spendings_to_register = []
        self.set_combobox_payment()
        self.set_handlers()
        today = date.today()
        self.dateEdit_created_date.setDate(QtCore.QDate(today.year, today.month,today.day))
        
        if 'spending_info' in window_params:
            spending_info     = window_params["spending_info"]
            if  spending_info:
                self.render_spending(spending_info)
                self.disable_form()

        #current_iva = SettingsController.get_iva()
        #self.lineEdit_IVA.setText(_fromUtf8(str(current_iva)))
        #self.is_manager = False
 
    def add_payment(self):

        payment_type = self.comboBox_payment.currentText()
        _num_row = self.table_payment.rowCount()
        #_num_row  = 0 if _current_num_row <1 else _current_num_row - 1
        self.table_payment.setRowCount(_num_row + 1)
        item = QtGui.QTableWidgetItem()
        item.setText(payment_type)
        self.table_payment.setVerticalHeaderItem(_num_row, item)
        item = QtGui.QTableWidgetItem("0")
        self.table_payment.setItem(_num_row,0,item)

        if payment_type == 'Efectivo':
            item = QtGui.QTableWidgetItem()
            self.table_payment.setItem(_num_row,1,item)
            item.setFlags(item.flags() & QtCore.Qt.ItemIsEnabled)
            item = QtGui.QTableWidgetItem()
            self.table_payment.setItem(_num_row,2,item)
            item.setFlags(item.flags() & QtCore.Qt.ItemIsEnabled)

        elif payment_type == 'Tarjeta':
            item = QtGui.QTableWidgetItem(0)
            self.table_payment.setItem(_num_row,2,item)
            item.setFlags(item.flags() & QtCore.Qt.ItemIsEnabled)

        item = QtGui.QTableWidgetItem("Eliminar")
        self.table_payment.setItem(_num_row,3,item)
        item.setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter|QtCore.Qt.AlignCenter)
        self.table_payment.itemDoubleClicked.connect(self.delete_payment)

    def delete_payment(self,item):
        _row = item.row()
        _column = item.column()
        _max_column = self.table_payment.columnCount()
        if not _column==3:
            return

        self.table_payment.removeRow(_row)

    def delete_spending(self,item):
        _row = item.row()
        _column = item.column()
        _max_column = self.table_spendings.columnCount()
        if not _column==3:
            return

        self.table_spendings.removeRow(_row)


    def add_spending(self):

        spending_params = {}
        spending_params['beneficiary']  = str(self.lineEdit_beneficiary.text())
        spending_params['ci_rif']       = str(self.lineEdit_ci_rif.text())
        spending_params['concept']      = str( self.lineEdit_concept.text())
        created_date = datetime.strptime(str(self.dateEdit_created_date.text()) , '%m/%d/%Y')
        spending_params['created_date'] = created_date
        spending_params['cashier_number'] = self.cashier_number

        _max_row = self.table_payment.rowCount()
        _max_column = self.table_payment.columnCount() - 1 
        payments = []
        pay_att = ['amount','bank','confirmation_number']
        total_amount = 0
        for row in range(0,_max_row):
            payment = {}
            payment['type'] = str(self.table_payment.verticalHeaderItem(row).text())
            for column in range(0,_max_column):
                item = self.table_payment.item(row,column)
                if item:
                    if column==0:
                        value = int(item.text())
                        total_amount += value

                    value = str(item.text())
                    payment[pay_att[column]] = value

            payments.append(payment)


        self.spendings_to_register.append({'spending':spending_params,
                               'payments':payments}) 
        _num_row = self.table_spendings.rowCount()
        self.table_spendings.setRowCount(_num_row + 1)
        item = QtGui.QTableWidgetItem()
        self.table_spendings.setVerticalHeaderItem(_num_row, item)

        item = QtGui.QTableWidgetItem(spending_params['beneficiary'])
        self.table_spendings.setItem(_num_row,0,item)
        item.setFlags(item.flags() & QtCore.Qt.ItemIsEnabled)
        item = QtGui.QTableWidgetItem(spending_params['ci_rif'])
        self.table_spendings.setItem(_num_row,1,item)
        item.setFlags(item.flags() & QtCore.Qt.ItemIsEnabled)
        item = QtGui.QTableWidgetItem(str(total_amount))
        self.table_spendings.setItem(_num_row,2,item)
        item.setFlags(item.flags() & QtCore.Qt.ItemIsEnabled)
        item = QtGui.QTableWidgetItem("Eliminar")
        self.table_spendings.setItem(_num_row,3,item)
        item.setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter|QtCore.Qt.AlignCenter)
        self.table_spendings.itemDoubleClicked.connect(self.delete_spending)



    def add_spendings(self):
        spendings = self.spendings_to_register
        from controllers import SpendingController
        for spending_info in spendings:
            spending   = spending_info['spending']
            payments = spending_info['payments']
            SpendingController.new_spending(spending,payments)
        self.spendings_to_register = []
        self.clear_content()

    def clear_content(self):
        self.table_spendings.clearContents()
        self.table_payment.clearContents()
        self.table_payment.setRowCount(0)

    def main(self):
        self.show()

    def set_combobox_payment(self):

        #self.table_payment.setVerticalHeaderLabels('Hola')
        categories = ['Efectivo','Trans/Cheq','Tarjeta']
        for category in categories:
            self.comboBox_payment.addItem(category)

    def set_handlers(self):
        self.btn_add_spending.clicked.connect(self.add_spending)
        self.btn_add_payment.clicked.connect(self.add_payment)    
        self.btn_register_spending.clicked.connect(self.add_spendings)

#/*****************************Render Methods*****************************/#

    def render_spending(self,spending_params):
        self.lineEdit_beneficiary.setText(spending_params.beneficiary)
        self.lineEdit_ci_rif.setText(str(spending_params.ci_rif))
        self.lineEdit_concept.setText(spending_params.concept)
        _date = spending_params.created_date
        date  = QtCore.QDate(_date.year,_date.month,_date.day)
        self.dateEdit_created_date.setDate(date)
        self.render_payments(spending_params.payments)

    def render_payments(self,payments):
        att_to_render = ['amount','bank','confirmation_number']
        vertical_header = 'type'
        Utils.render_items_list(self.table_payment,payments,att_to_render,vertical_header)

    def disable_form(self):
        self.lineEdit_beneficiary.setReadOnly(True)
        self.lineEdit_ci_rif.setReadOnly(True)
        self.lineEdit_concept.setReadOnly(True)
        self.dateEdit_created_date.setReadOnly(True)
        self.btn_add_payment.hide()
        self.btn_add_spending.hide()
        self.btn_register_spending.hide()
        self.comboBox_payment.hide()
        self.table_spendings.hide()




if __name__=='__main__':
    app = QtGui.QApplication(sys.argv)
    SpendingForm = SpendingForm()
    SpendingForm.main()
    app.exec_()
