#!/usr/bin/python
#coding=utf-8
 
from PyQt4 import QtGui, QtCore
from datetime import datetime

import sys
import Constants

 
# Import the interface class
import ProductFormUI


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

 
class ProductForm(QtGui.QDialog, ProductFormUI.Ui_Dialog):
    """ The second parent must be 'Ui_<obj. name of main widget class>'.
        If confusing, simply open up ImageViewer.py and get the class
        name used. I'd named mine as mainWindow, hence Ui_mainWindow. """
 
    def __init__(self, window_params=None, parent=None):
        super(ProductForm, self).__init__(parent)
        # This is because Python does not automatically
        # call the parent's constructor.
        self.setupUi(self)
        # Pass this "self" for building widgets and
        # keeping a reference.

        self.set_combobox_category()
        self.is_manager = window_params["is_manager"]
        product_info = window_params["product_info"]

        if not self.is_manager:
            product_info = window_params["product_info"]
            self.render_product(product_info)
            self.disable_form()
        else:
            if product_info:
                self.render_product(product_info)
                self.set_edit_product()
                self.is_edit = True
            else:
                self.is_edit = False
                self.set_form_newproduct()
 
    def main(self):
        self.show()

    #Rewrite fuction accept
    def accept(self):
        product_params = {}
        product_params["name"]   = str(self.lineEdit_name.text())
        product_params["price"]  = int(self.lineEdit_price.text())
        product_params["amount"] = int(self.lineEdit_amount.text())
        if self.is_edit:
            product_params["id"]   = str(self.lineEdit_code.text())

        from controllers import ProductController
        
        new_product = ProductController.new_product(product_params)

        msg = 'Editado' if self.is_edit else 'Agregado'
        if new_product:
            QtGui.QMessageBox.about(self,_fromUtf8("Notificación"),_fromUtf8("Producto "+msg))
            self.done(Constants.OBJECT_CREATED)
        else:
            QtGui.QMessageBox.about(self,_fromUtf8("Notificación"),_fromUtf8("Product No "+msg))

    def render_product(self,product_info):
        self.lineEdit_name.setText(product_info.name)
        self.lineEdit_code.setText(str(product_info.id))
        self.lineEdit_price.setText(str(product_info.price))        
        self.lineEdit_amount.setText(str(product_info.amount))

    def disable_form(self):
        self.lineEdit_name.setReadOnly(True)
        self.lineEdit_code.setReadOnly(True)
        self.lineEdit_price.setReadOnly(True)
        self.lineEdit_amount.setReadOnly(True)
        self.buttonBox.hide()

    def set_edit_product(self):
        self.lineEdit_code.setReadOnly(True)

    def set_form_newproduct(self):
        self.lineEdit_code.setEnabled(False)

    def set_combobox_category(self):
        from controllers import ProductController
        categories = ProductController.product_category()
        for category in categories:
            self.comboBox_category.addItem(category)
 
if __name__=='__main__':
    app = QtGui.QApplication(sys.argv)
    ProductForm = ProductForm()
    ProductForm.main()
    app.exec_()
    # This shows the interface we just created. No logic has been added, yet.