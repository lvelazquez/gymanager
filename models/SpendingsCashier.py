#!/usr/bin/python
#coding=utf-8
 
from PyQt4 import QtGui, QtCore
from Utils import *
from datetime import datetime,date
import Constants
import sys



try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

 
# Import the interface class
import SpendingsCashierUI


 
class SpendingsCashier(QtGui.QDialog, SpendingsCashierUI.Ui_Dialog):
    """ The second parent must be 'Ui_<obj. name of main widget class>'.
        If confusing, simply open up ImageViewer.py and get the class
        name used. I'd named mine as mainWindow, hence Ui_mainWindow. """

 
    def __init__(self, params=None,parent=None):
        super(SpendingsCashier,self).__init__(parent)
        # This is because Python does not automatically
        # call the parent's constructor.
        self.setupUi(self)


        self.spendings = None
        #Settings click handlers

        #self.productTable.clicked.connect(self.select_product)
        #self.lineEdit_search.returnPressed.connect(self.search_product)
        self.cashier_number = params['cashier_number']
        self.is_manager = True if params['user'] == Constants.MANAGER else False

        #if self.is_manager:
        #    self.new_product_btn.clicked.connect(self.new_product)
        #else:
        #    self.new_product_btn.hide()

        #set client list
        self.set_spendings_table()
        self.set_hanlders()
        #from controllers import SpendingController
        #SpendingController.total_price(ss)
        #SpendingController.today_spendings()

        # Pass this "self" for building widgets and
        # keeping a reference.

 
    def main(self):
        self.show()

    def select_spending(self,itemclicked):

        #selected row index
        row = itemclicked.row()
        max_column = self.spendings_table.columnCount() - 1
        range_selected = QtGui.QTableWidgetSelectionRange(row,0,row,max_column)
        self.spendings_table.setRangeSelected(range_selected,True)

        spending_code = str(self.spendings_table.item(row,0).text())
        return spending_code

    def view_selected_spending(self,itemclicked):

        from controllers import SpendingController
        
        spending_code = self.select_spending(itemclicked)
        index = map(lambda x:x.id,self.spendings).index(int(spending_code))
        selected_spending = self.spendings[index]
        #selected_product = ProductController.search_product(code=product_code)

        self.open_spendingform_window(selected_spending)

    #set initial client table configuration
    def set_spendings_table(self):

        #render client list
        self.render_spendings_list()
        today = date.today()
        self.dateEdit_search.setDate(QtCore.QDate(today.year, today.month,today.day))


        #set no editable
        #if not self.is_manager:
        self.spendings_table.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)

    def set_hanlders(self):
        self.lineEdit_search.returnPressed.connect(self.search_spending)
        self.dateEdit_search.\
        dateChanged.connect(self.search_by_date)
        self.spendings_table.doubleClicked.connect(self.view_selected_spending)


    #render complete client list
    def render_spendings_list(self):
        from controllers import SpendingController
        today = date.today()
        spendings = SpendingController.search_by_date(today,self.cashier_number) if not self.spendings else self.spendings
        self.spendings = spendings
        atts_to_render = SpendingController.attributes_to_render()
        Utils.render_items_list(self.spendings_table,spendings,atts_to_render)
        total = sum(map(lambda x:x.total,spendings))
        self.total_spendings.display(total)

    def render_matched_spendings(self,spendings_items):
        Utils.render_matched_items(self.spendings_table,spendings_items)


    #Handler for new_client_btn click
    def new_product(self):
        result = self.open_product_window()
        self.render_product_list()

    def search_spending(self):

        search_string = self.lineEdit_search.text()
        #if search string is empty, render complete client table
        self.render_spendings_list()            
        if not search_string:
            return

        matched_spendings = self.spendings_table.findItems(search_string, QtCore.Qt.MatchContains)

        if matched_spendings:
            self.render_matched_spendings(matched_spendings)
        
    def search_by_date(self):
        search_date = datetime.strptime(str(self.dateEdit_search.text()) , '%m/%d/%Y')
        from controllers import SpendingController
        spendings = SpendingController.search_by_date(search_date,self.cashier_number)
        atts_to_render = SpendingController.attributes_to_render()
        Utils.render_items_list(self.spendings_table,spendings,atts_to_render)



    def open_spendingform_window(self,spending_info=None):
        from SpendingForm import SpendingForm
        #app = QtGui.QApplication(sys.argv)
        window_params = {}
        window_params["is_manager"]   = self.is_manager
        window_params["spending_info"] = spending_info
        SpendingForm = SpendingForm(window_params)
        SpendingForm.exec_()





 
if __name__=='__main__':
    app = QtGui.QApplication(sys.argv)
    SpendingsCashier = SpendingsCashier()
    SpendingsCashier.main()
    app.exec_()
    # This shows the interface we just created. No logic has been added, yet.