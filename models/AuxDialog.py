#!/usr/bin/python
#coding=utf-8
 
from PyQt4 import QtGui, QtCore

import sys
 
# Import the interface class
import AuxDialogUI


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

 
class AuxDialog(QtGui.QDialog, AuxDialogUI.Ui_Dialog):
    """ The second parent must be 'Ui_<obj. name of main widget class>'.
        If confusing, simply open up ImageViewer.py and get the class
        name used. I'd named mine as mainWindow, hence Ui_mainWindow. """
 
    def __init__(self, window_params=None, parent=None):
        super(AuxDialog, self).__init__(parent)
        # This is because Python does not automatically
        # call the parent's constructor.
        self.setupUi(self)
        # Pass this "self" for building widgets and
        # keeping a reference.
        if 'product' in window_params:
            self.set_price_dialog(window_params['product'])
 
    def main(self):
        self.show()

    #Rewrite fuction accept
    def accept(self):
        new_price = int(self.lineEdit_price.text())
        self.done(new_price)



    def set_price_dialog(self,product):
        self.product_label.setText(product["name"])
        self.lineEdit_price.setText(product["price"])
