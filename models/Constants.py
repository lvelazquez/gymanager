#################CONSTANTS##########################
OBJECT_CREATED = 1
OBJECT_NOT_CREATED = 0




#######################################
CASHIER = 'Cashier'
MANAGER = 'Manager'


CREATED_PRODUCT = 'Producto Agregado'
CREATED_CLIENT  = 'Cliente Agregado'
EDITED_PRODUCT  = 'Producto Editado'
EDITED_CLIENT   = 'Cliente Editado'

NOT_CREATED_PRODUCT = 'Producto No Agregado'
NOT_CREATED_CLIENT  = 'Cliente  No Agregado'
NOT_EDITED_PRODUCT  = 'Producto No Editado'
NOT_EDITED_CLIENT   = 'Cliente  No Editado'

