#!/usr/bin/python
#coding=utf-8
 
from PyQt4 import QtGui, QtCore

import sys
import Constants
 
# Import the interface class
import MainWindowUI


try:
	_fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
	def _fromUtf8(s):
		return s

 
class MainWindow(QtGui.QMainWindow, MainWindowUI.Ui_MainWindow):
	""" The second parent must be 'Ui_<obj. name of main widget class>'.
		If confusing, simply open up ImageViewer.py and get the class
		name used. I'd named mine as mainWindow, hence Ui_mainWindow. """
 
	def __init__(self, window_params=None, parent=None):
		super(MainWindow, self).__init__(parent)
		# This is because Python does not automatically
		# call the parent's constructor.
		self.setupUi(self)
		# Pass this "self" for building widgets and
		# keeping a reference.

		self.user_type = Constants.MANAGER

		self.clients_btn.clicked.connect(self.open_clients_window)
		self.products_btn.clicked.connect(self.open_products_window)
		self.settings_btn.clicked.connect(self.open_settings_window)
		self.spendings_btn.clicked.connect(self.open_spendings_window)
		self.add_spending_btn.clicked.connect(self.open_addspending_window)
		self.billing_btn.clicked.connect(self.open_billingform_window)
		self.sales_btn.clicked.connect(self.open_sales_window)
		self.products_report_btn.clicked.connect(self.open_products_report_window)
		self.is_manager = True if self.user_type == Constants.MANAGER else False

		if not self.is_manager:
			self.set_cashier_functions()
		self.cashier_number = 2

 
	def main(self):
		self.show()

	def open_clients_window(self,params=None):
		from Clients import Clients
		#app = QtGui.QApplication(sys.argv)
		params  = {}
		params["user"] = self.user_type
		Clients = Clients(params)
		Clients.exec_()

	#cambiar esto  para que pida el password y si se acepta, se abra directamente
	#la ventana de settings desde la ventana principal
	def open_settings_window(self,params=None):
		from passwordSetting import PasswordSetting
		#app = QtGui.QApplication(sys.argv)
		PasswordSetting = PasswordSetting()
		PasswordSetting.exec_()

	def open_products_window(self,params=None):
		from Products import Products
		#app = QtGui.QApplication(sys.argv)
		params  = {}
		params["user"] = self.user_type
		Products = Products(params)
		Products.exec_()

	def open_products_report_window(self,params=None):
		from ProductsReport import ProductsReport
		params  = {}
		params["user"] = self.user_type
		ProductsReport = ProductsReport(params)
		ProductsReport.exec_()



	def open_spendings_window(self,params=None):



		if not self.is_manager:
			from SpendingsCashier import SpendingsCashier 
			#app = QtGui.QApplication(sys.argv)
			params  = {}
			params["user"] = self.user_type
			params["cashier_number"] = self.cashier_number
			SpendingsCashier = SpendingsCashier(params)
			SpendingsCashier.exec_()
		else:
			from SpendingsManager import SpendingsManager 
			params  = {}
			params["user"] = self.user_type
			SpendingsManager = SpendingsManager(params)
			SpendingsManager.exec_()


	def open_addspending_window(self,params=None):


		from SpendingForm import SpendingForm 
		#app = QtGui.QApplication(sys.argv)
		params  = {}
		params["cashier_number"] = self.cashier_number
		params["user"] = self.user_type
		SpendingForm = SpendingForm(params)
		SpendingForm.exec_()

	def open_billingform_window(self,params=None):
		from BillingForm import BillingForm
		#app = QtGui.QApplication(sys.argv)
		params  = {}
		params["user"] = self.user_type
		params["cashier_number"] = self.cashier_number
		BillingForm = BillingForm(params)
		BillingForm.exec_()

	def open_sales_window(self,params=None):

		if not self.is_manager:
			from SalesCashier import SalesCashier
			
			#app = QtGui.QApplication(sys.argv)
			params  = {}
			params["user"] = self.user_type
			params["cashier_number"] = self.cashier_number
			SalesCashier = SalesCashier(params)
			SalesCashier.exec_()

		if self.is_manager:
			from SalesManager import SalesManager
			params  = {}
			params["user"] = self.user_type
			SalesManager = SalesManager(params)
			SalesManager.exec_()


	def set_cashier_functions(self):
		self.products_report_btn.hide()
	

if __name__=='__main__':
	app = QtGui.QApplication(sys.argv)
	MainWindow = MainWindow()
	MainWindow.main()
	app.exec_()
