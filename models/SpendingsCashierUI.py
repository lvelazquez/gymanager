# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'SpendingsCashierUI.ui'
#
# Created: Thu Jun 20 22:16:34 2013
#      by: PyQt4 UI code generator 4.10
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.setWindowModality(QtCore.Qt.NonModal)
        Dialog.resize(718, 596)
        self.spendings_table = QtGui.QTableWidget(Dialog)
        self.spendings_table.setGeometry(QtCore.QRect(30, 180, 651, 191))
        self.spendings_table.setObjectName(_fromUtf8("spendings_table"))
        self.spendings_table.setColumnCount(4)
        self.spendings_table.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.spendings_table.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.spendings_table.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.spendings_table.setHorizontalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.spendings_table.setHorizontalHeaderItem(3, item)
        self.spendings_table.horizontalHeader().setCascadingSectionResizes(False)
        self.spendings_table.horizontalHeader().setDefaultSectionSize(162)
        self.spendings_table.verticalHeader().setVisible(False)
        self.line = QtGui.QFrame(Dialog)
        self.line.setGeometry(QtCore.QRect(30, 120, 651, 20))
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.label = QtGui.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(30, 150, 131, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayoutWidget = QtGui.QWidget(Dialog)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(440, 140, 241, 31))
        self.horizontalLayoutWidget.setObjectName(_fromUtf8("horizontalLayoutWidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label_2 = QtGui.QLabel(self.horizontalLayoutWidget)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout.addWidget(self.label_2)
        self.lineEdit_search = QtGui.QLineEdit(self.horizontalLayoutWidget)
        self.lineEdit_search.setObjectName(_fromUtf8("lineEdit_search"))
        self.horizontalLayout.addWidget(self.lineEdit_search)
        self.horizontalLayoutWidget_2 = QtGui.QWidget(Dialog)
        self.horizontalLayoutWidget_2.setGeometry(QtCore.QRect(520, 390, 160, 41))
        self.horizontalLayoutWidget_2.setObjectName(_fromUtf8("horizontalLayoutWidget_2"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.horizontalLayoutWidget_2)
        self.horizontalLayout_2.setMargin(0)
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label_3 = QtGui.QLabel(self.horizontalLayoutWidget_2)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_2.addWidget(self.label_3)
        self.total_spendings = QtGui.QLCDNumber(self.horizontalLayoutWidget_2)
        self.total_spendings.setObjectName(_fromUtf8("total_spendings"))
        self.horizontalLayout_2.addWidget(self.total_spendings)
        self.horizontalLayoutWidget_3 = QtGui.QWidget(Dialog)
        self.horizontalLayoutWidget_3.setGeometry(QtCore.QRect(40, 390, 162, 31))
        self.horizontalLayoutWidget_3.setObjectName(_fromUtf8("horizontalLayoutWidget_3"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout(self.horizontalLayoutWidget_3)
        self.horizontalLayout_3.setMargin(0)
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.label_4 = QtGui.QLabel(self.horizontalLayoutWidget_3)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.horizontalLayout_3.addWidget(self.label_4)
        self.dateEdit_search = QtGui.QDateEdit(self.horizontalLayoutWidget_3)
        self.dateEdit_search.setDate(QtCore.QDate(2013, 1, 31))
        self.dateEdit_search.setCalendarPopup(True)
        self.dateEdit_search.setObjectName(_fromUtf8("dateEdit_search"))
        self.horizontalLayout_3.addWidget(self.dateEdit_search)
        self.pushButton = QtGui.QPushButton(Dialog)
        self.pushButton.setEnabled(False)
        self.pushButton.setGeometry(QtCore.QRect(280, 390, 131, 31))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Dialog", None))
        item = self.spendings_table.horizontalHeaderItem(0)
        item.setText(_translate("Dialog", "Código", None))
        item = self.spendings_table.horizontalHeaderItem(1)
        item.setText(_translate("Dialog", "Beneficiario", None))
        item = self.spendings_table.horizontalHeaderItem(2)
        item.setText(_translate("Dialog", "Monto", None))
        item = self.spendings_table.horizontalHeaderItem(3)
        item.setText(_translate("Dialog", "Fecha", None))
        self.label.setText(_translate("Dialog", "Gastos Diarios", None))
        self.label_2.setText(_translate("Dialog", "Búsqueda", None))
        self.label_3.setText(_translate("Dialog", "Total", None))
        self.label_4.setText(_translate("Dialog", "Fecha", None))
        self.dateEdit_search.setDisplayFormat(_translate("Dialog", "M/d/yyyy", None))
        self.pushButton.setText(_translate("Dialog", "Nota de Credito", None))

