#!/usr/bin/python
#coding=utf-8
 
from PyQt4 import QtGui, QtCore
from Utils import *
from datetime import datetime,date
import Constants
import sys
import sys



try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

 
# Import the interface class
import SalesCashierUI


 
class SalesCashier(QtGui.QDialog, SalesCashierUI.Ui_Dialog):
    """ The second parent must be 'Ui_<obj. name of main widget class>'.
        If confusing, simply open up ImageViewer.py and get the class
        name used. I'd named mine as mainWindow, hence Ui_mainWindow. """

 
    def __init__(self, params=None,parent=None):
        super(SalesCashier,self).__init__(parent)
        # This is because Python does not automatically
        # call the parent's constructor.
        self.setupUi(self)


        self.sales = None
        #Settings click handlers

        #self.productTable.clicked.connect(self.select_product)
        #self.lineEdit_search.returnPressed.connect(self.search_product)
        #params = {}
        #params['user'] = Constants.MANAGER
        self.is_manager = True if params['user'] == Constants.MANAGER else False

        #if self.is_manager:
        #    self.new_product_btn.clicked.connect(self.new_product)
        #else:
        #    self.new_product_btn.hide()

        #set client list
        self.cashier_number = params['cashier_number']
        self.set_sales_table()
        self.set_hanlders()

        #from controllers import SpendingController
        #SpendingController.total_price(ss)
        #SpendingController.today_spendings()

        # Pass this "self" for building widgets and
        # keeping a reference.

 
    def main(self):
        self.show()

    def select_sale(self,itemclicked):

        #selected row index
        row = itemclicked.row()
        row = itemclicked.row()
        max_column = self.sales_table.columnCount() - 1
        range_selected = QtGui.QTableWidgetSelectionRange(row,0,row,max_column)
        self.sales_table.setRangeSelected(range_selected,True)

        sale_code = str(self.sales_table.item(row,1).text())
        self.selected_sale = sale_code
        self.delete_sale_btn.setEnabled(True)
        return sale_code

    def view_selected_sale(self,itemclicked):

        from controllers import SalesController
        
        sales_code = self.select_sale(itemclicked)
        index = map(lambda x:x.id,self.sales).index(int(sales_code))
        selected_spending = self.sales[index]
        #selected_product = ProductController.search_product(code=product_code)

        #self.open_spendingform_window(selected_spending)

    #set initial client table configuration
    def set_sales_table(self):

        #render client list
        self.render_sales_list()
        today = date.today()
        self.dateEdit_search.setDate(QtCore.QDate(today.year, today.month,today.day))


        #set no editable
        #if not self.is_manager:
        self.sales_table.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)

    def set_hanlders(self):
        self.lineEdit_search.returnPressed.connect(self.search_sales)
        self.dateEdit_search.\
        dateChanged.connect(self.search_by_date)
        self.sales_table.doubleClicked.connect(self.view_selected_sale)
        self.sales_table.clicked.connect(self.select_sale)
        self.delete_sale_btn.clicked.connect(self.delete_sale)


    #render complete client list
    def render_sales_list(self):
        from controllers import SalesController
        today = date.today()
        sales = SalesController.search_by_date(today,self.cashier_number) if not self.sales else self.sales
        self.sales = sales

        items = []
        for sale in sales:
            items.append(Utils.set_att(sale))
        
        atts_to_render = ['date','id','client','product_category','product_name','price']
        Utils.render_items_list(self.sales_table,items,atts_to_render)
        total = sum(map(lambda x:x['price'],sales))
        self.total_sales.display(total)

    def render_matched_sales(self,sales_items):
        Utils.render_matched_items(self.sales_table,sales_items)


    def delete_sale(self):
        sale_code = self.selected_sale
        from controllers import SalesController
        deleted = SalesController.delete_sale(sale_code)
        if not deleted:
            QtGui.QMessageBox.about(self,_fromUtf8("Notificación"),
                                   _fromUtf8("No se pudo hacer nota de debito."))
            return
        QtGui.QMessageBox.about(self,_fromUtf8("Notificación"),
                                   _fromUtf8("Nota de debito hecha"))
        self.sales = None
        self.render_sales_list()




    def search_sales(self):

        search_string = self.lineEdit_search.text()
        #if search string is empty, render complete client table
        self.render_sales_list()            
        if not search_string:
            return

        matched_spendings = self.sales_table.findItems(search_string, QtCore.Qt.MatchContains)

        if matched_spendings:
            self.render_matched_sales(matched_spendings)
        
    def search_by_date(self):
        from controllers import SalesController
        search_date = datetime.strptime(str(self.dateEdit_search.text()) , '%m/%d/%Y')
        sales = SalesController.search_by_date(search_date,self.cashier_number)
        items = []
        for sale in sales:
            items.append(Utils.set_att(sale))
        atts_to_render = ['date','id','client','product_category','product_name','price']
        Utils.render_items_list(self.sales_table,items,atts_to_render)



    def open_spendingform_window(self,spending_info=None):
        from SpendingForm import SpendingForm
        #app = QtGui.QApplication(sys.argv)
        window_params = {}
        window_params["is_manager"]   = self.is_manager
        window_params["spending_info"] = spending_info
        SpendingForm = SpendingForm(window_params)
        SpendingForm.exec_()



