#!/usr/bin/python
#coding=utf-8
 
from PyQt4 import QtGui, QtCore
from Utils import *
import Constants
import sys
from datetime import datetime,date,timedelta



try:
	_fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
	def _fromUtf8(s):
		return s

 
# Import the interface class
import BillingFormUI


 
class BillingForm(QtGui.QDialog, BillingFormUI.Ui_Dialog):
	""" The second parent must be 'Ui_<obj. name of main widget class>'.
		If confusing, simply open up ImageViewer.py and get the class
		name used. I'd named mine as mainWindow, hence Ui_mainWindow. """

 
	def __init__(self, params,parent=None):
		super(BillingForm,self).__init__(parent)
		# This is because Python does not automatically
		# call the parent's constructor.
		self.setupUi(self)

		self.set_handlers()
		#self.products = None
		#Settings click handlers

		#self.productTable.clicked.connect(self.select_product)
		#self.productTable.doubleClicked.connect(self.view_selected_product)
		#self.lineEdit_search.returnPressed.connect(self.search_product)
		
		#self.is_manager = True if params['user'] == Constants.MANAGER else False
		self.is_manager = False
		#if self.is_manager:
		#    self.new_product_btn.clicked.connect(self.new_product)
		#else:
		#    self.new_product_btn.hide()

		#set client list
		self.clients = None
		self.has_inscription = False
		self.membership_type = None
		self.total_sales = 0
		self.enrollments_numbers=0
		self.monthly_payment_number=0
		self.cashier_number = params['cashier_number']

		#associative dict beetween product added and sales table
		self._row_products = []

		#current client
		self._current_client = None

		#self.current_products_listed = {}
		self.set_client_table()
		self.set_category_combobox()
		self.set_payment_combobox()
		self.set_product_table()
		self.set_sales_table()
		self.set_client_form()
		# Pass this "self" for building widgets and
		# keeping a reference.

 
	def main(self):
		self.show()



	def view_selected_product(self,itemclicked):

		from controllers import ProductController
		
		product_code = self.select_product(itemclicked)
		selected_product = ProductController.search_product(code=product_code)

		self.open_product_window(selected_product)





#/************************RENDER METHOS********************/#

	#render complete client list
	def render_client_list(self):
		from controllers import ClientController

		clients = ClientController.all_clients() if not self.clients else self.clients
		self.clients = clients
		atts_to_render = ['name','ci']

		Utils.render_items_list(self.clients_table,clients,atts_to_render)

	def render_matched_items(self,table,matched_items):

		Utils.render_matched_items(table,matched_items)

	def render_client_form(self,client):
		#print client
		if client.membership == 'mensualidad sin inscripcion' or not client.membership:
			self.dateEdit_sod.setStyleSheet(_fromUtf8("background-color:rgb(255, 0, 0);\
				                                       color: rgb(255, 255, 255);"))
		else:
			self.dateEdit_sod.setStyleSheet(_fromUtf8("background-color: rgb(85, 255, 0);\
				                                       color: rgb(0, 0, 0);"))			
		_sod = client.signout_date
		if _sod:
			self.dateEdit_sod.setDate(QtCore.QDate(_sod.year, _sod.month,_sod.day))
		self.lineEdit_name.setText(client.name)
		self.lineEdit_ci.setText(str(client.ci))


	def render_products(self,category):
		from controllers import ProductController
		products = ProductController.get_products_category(category)
		self.products = products
		if not products:
			return 
		atts_to_render = ['id','name','price']

		Utils.render_items_list(self.products_table,products,atts_to_render)

	def render_sale(self,product):

		row = [product.name,product.price]
		Utils.insert_row(self.sales_table,row)

	# #Handler for new_client_btn click
	# def new_product(self):
	#     result = self.open_product_window()
	#     self.render_product_list()

#/************************MAIN METHODS********************/#

	def select_client(self,itemclicked):

		#selected row index
		row = itemclicked.row()
		max_column = self.clients_table.columnCount() - 1
		range_selected = QtGui.QTableWidgetSelectionRange(row,0,row,max_column)
		self.clients_table.setRangeSelected(range_selected,True)

		client_ci = str(self.clients_table.item(row,1).text())

		from controllers import ClientController
		
		selected_client = ClientController.search_client(ci=client_ci)
		if self.has_inscription and selected_client.membership=='Mensualidad' and self.enrollments_numbers<2:
			msg = 'Ya este cliente está inscrito'
			QtGui.QMessageBox.about(self,_fromUtf8("Notificación"),_fromUtf8(msg))            
			return
		self._current_client = selected_client
		self.render_client_form(selected_client)
		self.register_sale_btn.setEnabled(True)


	def select_category(self,category_index):

		#selected row index
		category = self.comboBox_category.itemText(category_index)
		self.render_products(category)

	def select_product(self,itemclicked):

		#selected row index
		row = itemclicked.row()
		max_column = self.products_table.columnCount() - 1
		range_selected = QtGui.QTableWidgetSelectionRange(row,0,row,max_column)
		self.products_table.setRangeSelected(range_selected,True)

		product_code = str(self.products_table.item(row,0).text())
		self.current_product_code = product_code

	def select_sale(self,itemclicked):
		
		row = itemclicked.row()
		max_column = self.sales_table.columnCount() - 1
		range_selected = QtGui.QTableWidgetSelectionRange(row,0,row,max_column)
		self.sales_table.setRangeSelected(range_selected,True)
		self.current_sale_row = row

	def select_payment(self,itemclicked):
		
		row = itemclicked.row()
		max_column = self.table_payment.columnCount() - 1
		range_selected = QtGui.QTableWidgetSelectionRange(row,0,row,max_column)
		self.table_payment.setRangeSelected(range_selected,True)
		self.current_payment_row = row


	def add_sale(self,itemclicked):
		product_code = self.current_product_code
		selected_product = None

		for product in self.products:
			if product.id == int(product_code):
			

				if not product.category in ['Inscripcion','Planes']:
					selected_product = product   
					if product.category=='Mensualidad':
						self.monthly_payment_number += 1
						if self.membership_type == 'normal':
							product.price = self.adjust_membership_price(product.price)                
					self.total_sales += product.price
				elif product.category in ['Inscripcion','Planes'] and not self.has_inscription:
					if product.category == 'Inscripcion' and not self.has_inscription:
						_name = product.category
						membership_type = self.which_membership(_name)
						if not product.name=='1 Persona':
							membership_type = None
							self.enrollments_numbers = int(product.name.split(" ")[0])

					elif product.category == 'Planes':
						_name = product.name
						membership_type = self.which_membership(_name)

					self.enable_client_register(membership_type)
					selected_product  = product
					self.total_sales += product.price
				else:
					msg = 'Ya está registrando una Inscripcion'
					QtGui.QMessageBox.about(self,_fromUtf8("Notificación"),_fromUtf8(msg))
					return

		self.render_sale(selected_product)
		_row_count = self.sales_table.rowCount()
		self._row_products.append(selected_product)


		self.lcdNumber_total_sales.display(self.total_sales)

	def add_payment(self):

		payment_type = self.comboBox_payment.currentText()
		_num_row = self.table_payment.rowCount()
		self.table_payment.setRowCount(_num_row + 1)



		item = QtGui.QTableWidgetItem()
		item.setText(payment_type)
		self.table_payment.setVerticalHeaderItem(_num_row, item)


		if payment_type == 'Efectivo':

			if _num_row > 0:
				item = QtGui.QTableWidgetItem()
				self.table_payment.item(_num_row-1,0).setText("")
			else: 
				item = QtGui.QTableWidgetItem(str(self.total_sales))

			self.table_payment.setItem(_num_row,0,item)

			item = QtGui.QTableWidgetItem(self.total_sales)
			self.table_payment.setItem(_num_row,1,item)
			item.setFlags(item.flags() & QtCore.Qt.ItemIsEnabled)
			item = QtGui.QTableWidgetItem()
			self.table_payment.setItem(_num_row,2,item)
			item.setFlags(item.flags() & QtCore.Qt.ItemIsEnabled)

		elif payment_type == 'Tarjeta':

			if _num_row > 0:
				item = QtGui.QTableWidgetItem()
				self.table_payment.item(_num_row-1,0).setText("")
			else: 
				item = QtGui.QTableWidgetItem(str(self.total_sales))

			self.table_payment.setItem(_num_row,0,item)

			item = QtGui.QTableWidgetItem()
			self.table_payment.setItem(_num_row,2,item)
			item.setFlags(item.flags() & QtCore.Qt.ItemIsEnabled)




	def edit_sale_price(self):
		row = self.current_sale_row
		name  = self.sales_table.item(row,0).text()
		price = self.sales_table.item(row,1).text()
		from AuxDialog import AuxDialog
		window_params = {}
		product = {}
		product['name']  = name 
		product['price'] = price 
		window_params["product"]   = product 
		AuxDialog = AuxDialog(window_params)
		new_price = AuxDialog.exec_()
		self.sales_table.item(row,1).setText(str(new_price))
		self.set_total_amount()
		#self._row_products[row].price = new_price
		#print "Nuevo Precio",self._row_products[row].price


	def which_membership(self,product_name):
		membership_type = None
		if product_name == 'Inscripcion':
			membership_type = 'normal'
		elif product_name == 'Mensualidad sin Inscripcion':
			membership_type = 'mensualidad sin inscripcion'
		elif product_name == 'Diario':
			membership_type = 'diario'

		return membership_type

	def adjust_membership_price(self,membership_price):
		today = date.today()
		day_remaining = abs(30 - int(today.day))
		price = (membership_price*day_remaining)/30

		#round to 5
		base = 5
		price = int(base * round(float(price)/base))
		return price

	def check_payment(self,payments):
		total_amount = sum(map(lambda x: int(x['amount']),payments))
		if not total_amount == self.total_sales:
			msg = 'El monto pagado difiere del monto a pagar'
			QtGui.QMessageBox.about(self,_fromUtf8("Notificación"),_fromUtf8(msg))
			return 
		return total_amount

	def products_to_sale(self):
		_max_row = self.sales_table.rowCount()
		products_sale  = self._row_products
		_products= []

		for row in xrange(0,_max_row):
			product_params = {}
			product_params['product_general'] = products_sale[row]
			product_params['price'] = int(self.sales_table.item(row,1).text())
			_products.append(product_params)
		return _products


	def set_total_amount(self):

		_total_amount = 0
		_max_row = self.sales_table.rowCount()
		products_sale  = self._row_products
		for row in xrange(0,_max_row):
			_total_amount += int(self.sales_table.item(row,1).text())

		self.total_sales = _total_amount
		self.lcdNumber_total_sales.display(_total_amount)


	def search_clients(self):

		search_string = self.lineEdit_search_clients.text()
		#if search string is empty, render complete client table
		self.render_client_list()            
		if not search_string:
			return

		matched_clients = self.clients_table.findItems(search_string, QtCore.Qt.MatchContains)

		if matched_clients:
			self.render_matched_items(self.clients_table,matched_clients)

	def register_sale(self):
		sales_params = {}
		#get sales information
		sales_params['products'] = self.products_to_sale()

		current_client = self._current_client
		if not current_client:
			msg = 'Debe seleccionar un cliente'
			QtGui.QMessageBox.about(self,_fromUtf8("Notificación"),_fromUtf8(msg))
			return

		payments = Utils.get_payments_table(self.table_payment)
		valid_payment  = self.check_payment(payments)
		if not valid_payment:
			return
		sales_params['client']       = current_client
		sales_params['new_sod_date'] = datetime.strptime(str(self.dateEdit_sod.text()) , '%m/%d/%Y')
		sales_params['payments']     = payments
		sales_params['enrollments_numbers']    = self.enrollments_numbers
		sales_params['monthly_payment_number'] = self.monthly_payment_number
		sales_params['cashier_number'] = self.cashier_number
		if self.membership_type:
			sales_params['membership_type'] = self.membership_type
		else:
			sales_params['membership_type'] = None
		from controllers import SalesController
		new_sale = SalesController.register_sale(sales_params,payments)
		if not new_sale:
			msg = 'Venta No Registrada'
			QtGui.QMessageBox.about(self,_fromUtf8("Notificación"),_fromUtf8(msg))
			return
		self.clear_content()
		msg = 'Venta Registrada'
		QtGui.QMessageBox.about(self,_fromUtf8("Notificación"),_fromUtf8(msg))


	def register_client(self):
		self.open_register_client_window()



	def delete_sale(self):
		row = self.current_sale_row
		product = self._row_products.pop(row)
		if product.category in ['Inscripcion','Planes']:
			#allow put subscripcion again
			self.disable_client_register()
		self.sales_table.removeRow(row)
		self.set_total_amount()

	def delete_payment(self):
		row = self.current_payment_row
		self.table_payment.removeRow(row)


	def enable_client_register(self,membership_type):
		self.has_inscription = True
		self.membership_type = membership_type
		self.dateEdit_sod.setEnabled(True)
		self.register_client_btn.setEnabled(True)
		self.lineEdit_name.setText("")
		self.lineEdit_name.setEnabled(False)
		self.lineEdit_ci.setText("")
		self.lineEdit_ci.setEnabled(False)


	def disable_client_register(self):
		self.has_inscription = False
		self.membership_type = None
		self.register_client_btn.setEnabled(False)
		self.lineEdit_name.setText("")
		self.lineEdit_name.setEnabled(True)
		self.lineEdit_ci.setText("")
		self.lineEdit_ci.setEnabled(True)

	def clear_content(self):
		self.lineEdit_name.setText("")
		self.lineEdit_ci.setText("")
		self.sales_table.clearContents()
		self.sales_table.setRowCount(0)
		self.table_payment.clearContents()
		self.table_payment.setRowCount(0)



#/************************SETTINGS METHOS********************/#

	#set initial client table configuration
	def set_client_table(self):

		#set date today
		today = date.today()
		self.dateEdit_sod.setDate(QtCore.QDate(today.year, today.month,today.day))

		#render client list
		self.render_client_list()

		#set no editable
		self.clients_table.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
		
	def set_product_table(self):
		self.products_table.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)

	def set_sales_table(self):
		self.sales_table.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)


	def set_handlers(self):

		self.lineEdit_search_clients.returnPressed.connect(self.search_clients)
		self.clients_table.doubleClicked.connect(self.select_client)
		self.comboBox_category.activated.connect(self.select_category)

		self.products_table.clicked.connect(self.select_product)
		self.products_table.doubleClicked.connect(self.add_sale)
		self.sales_table.clicked.connect(self.select_sale)
		self.table_payment.clicked.connect(self.select_payment)
		self.delete_sale_btn.clicked.connect(self.delete_sale)
		self.edit_sale_btn.clicked.connect(self.edit_sale_price)
		self.register_sale_btn.clicked.connect(self.register_sale)
		self.register_client_btn.clicked.connect(self.register_client)
		self.add_payment_btn.clicked.connect(self.add_payment)    
		self.delete_payment_btn.clicked.connect(self.delete_payment)


	def set_category_combobox(self):
		from controllers import ProductController
		categories = ProductController.product_category()
		for category in categories:
			self.comboBox_category.addItem(category)

	def set_payment_combobox(self):

		#self.table_payment.setVerticalHeaderLabels('Hola')
		categories = ['Efectivo','Trans/Cheq','Tarjeta']
		for category in categories:
			self.comboBox_payment.addItem(category)

	def set_client_form(self):
		self.lineEdit_name.setReadOnly(True)
		self.lineEdit_ci.setReadOnly(True)

	def open_product_window(self,product_info=None):
		from ProductForm import ProductForm
		#app = QtGui.QApplication(sys.argv)
		window_params = {}
		window_params["is_manager"]   = self.is_manager
		window_params["product_info"] = product_info
		ProductForm = ProductForm(window_params)
		if ProductForm.exec_() == Constants.OBJECT_CREATED:
			self.render_product_list()

	def open_register_client_window(self,selected_client=None):
		from ClientForm import ClientForm
		#app = QtGui.QApplication(sys.argv)
		window_params = {}
		window_params["is_manager"]   = self.is_manager
		window_params["client_info"]  = selected_client
		window_params["from_billing"] = True
		ClientForm = ClientForm(window_params)
		if ClientForm.exec_() == Constants.OBJECT_CREATED:
			self.clients = None
			self.render_client_list()


#######################################################################################





 
if __name__=='__main__':
	app = QtGui.QApplication(sys.argv)
	BillingForm = BillingForm()
	BillingForm.main()
	app.exec_()
	# This shows the interface we just created. No logic has been added, yet.