# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainWindowUI.ui'
#
# Created: Sun Jun 16 20:11:05 2013
#      by: PyQt4 UI code generator 4.10
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(715, 575)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.gridLayoutWidget = QtGui.QWidget(self.centralwidget)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(40, 50, 614, 111))
        self.gridLayoutWidget.setObjectName(_fromUtf8("gridLayoutWidget"))
        self.gridLayout = QtGui.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setMargin(0)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.products_btn = QtGui.QCommandLinkButton(self.gridLayoutWidget)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("../png/24x24/Shopping cart.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.products_btn.setIcon(icon)
        self.products_btn.setObjectName(_fromUtf8("products_btn"))
        self.gridLayout.addWidget(self.products_btn, 0, 1, 1, 1)
        self.spendings_btn = QtGui.QCommandLinkButton(self.gridLayoutWidget)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8("../png/24x24/List.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.spendings_btn.setIcon(icon1)
        self.spendings_btn.setObjectName(_fromUtf8("spendings_btn"))
        self.gridLayout.addWidget(self.spendings_btn, 0, 2, 1, 1)
        self.clients_btn = QtGui.QCommandLinkButton(self.gridLayoutWidget)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(_fromUtf8("../png/24x24/User group.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.clients_btn.setIcon(icon2)
        self.clients_btn.setObjectName(_fromUtf8("clients_btn"))
        self.gridLayout.addWidget(self.clients_btn, 0, 0, 1, 1)
        self.add_spending_btn = QtGui.QCommandLinkButton(self.gridLayoutWidget)
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(_fromUtf8("../png/24x24/Add.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.add_spending_btn.setIcon(icon3)
        self.add_spending_btn.setObjectName(_fromUtf8("add_spending_btn"))
        self.gridLayout.addWidget(self.add_spending_btn, 1, 2, 1, 1)
        self.products_report_btn = QtGui.QCommandLinkButton(self.gridLayoutWidget)
        self.products_report_btn.setIcon(icon1)
        self.products_report_btn.setObjectName(_fromUtf8("products_report_btn"))
        self.gridLayout.addWidget(self.products_report_btn, 1, 1, 1, 1)
        self.frame = QtGui.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(40, 170, 621, 211))
        self.frame.setStyleSheet(_fromUtf8("background:rgb(255, 255, 127)"))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.settings_btn = QtGui.QPushButton(self.centralwidget)
        self.settings_btn.setGeometry(QtCore.QRect(520, 10, 131, 27))
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap(_fromUtf8("../png/24x24/Application.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.settings_btn.setIcon(icon4)
        self.settings_btn.setObjectName(_fromUtf8("settings_btn"))
        self.gridLayoutWidget_2 = QtGui.QWidget(self.centralwidget)
        self.gridLayoutWidget_2.setGeometry(QtCore.QRect(480, 390, 174, 41))
        self.gridLayoutWidget_2.setObjectName(_fromUtf8("gridLayoutWidget_2"))
        self.gridLayout_2 = QtGui.QGridLayout(self.gridLayoutWidget_2)
        self.gridLayout_2.setMargin(0)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.billing_btn = QtGui.QCommandLinkButton(self.gridLayoutWidget_2)
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap(_fromUtf8("../png/24x24/Calculator.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.billing_btn.setIcon(icon5)
        self.billing_btn.setObjectName(_fromUtf8("billing_btn"))
        self.gridLayout_2.addWidget(self.billing_btn, 0, 0, 1, 1)
        self.sales_btn = QtGui.QCommandLinkButton(self.centralwidget)
        self.sales_btn.setGeometry(QtCore.QRect(270, 390, 200, 52))
        icon6 = QtGui.QIcon()
        icon6.addPixmap(QtGui.QPixmap(_fromUtf8("../png/24x24/Dollar.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.sales_btn.setIcon(icon6)
        self.sales_btn.setObjectName(_fromUtf8("sales_btn"))
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 715, 25))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow", None))
        self.products_btn.setText(_translate("MainWindow", "Productos", None))
        self.spendings_btn.setText(_translate("MainWindow", "Listar Gastos", None))
        self.clients_btn.setText(_translate("MainWindow", "Clientes", None))
        self.add_spending_btn.setText(_translate("MainWindow", "Registrar Gastos", None))
        self.products_report_btn.setText(_translate("MainWindow", "Reporte Productos", None))
        self.settings_btn.setText(_translate("MainWindow", "Configuración", None))
        self.billing_btn.setText(_translate("MainWindow", "Facturar", None))
        self.sales_btn.setText(_translate("MainWindow", "Ventas", None))

