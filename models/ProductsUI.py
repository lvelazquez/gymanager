# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ProductsUI.ui'
#
# Created: Sun Jun 16 17:07:31 2013
#      by: PyQt4 UI code generator 4.10
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.setEnabled(True)
        Dialog.resize(670, 456)
        self.line = QtGui.QFrame(Dialog)
        self.line.setGeometry(QtCore.QRect(30, 20, 601, 20))
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.productTable = QtGui.QTableWidget(Dialog)
        self.productTable.setGeometry(QtCore.QRect(30, 70, 601, 321))
        self.productTable.setEditTriggers(QtGui.QAbstractItemView.DoubleClicked)
        self.productTable.setObjectName(_fromUtf8("productTable"))
        self.productTable.setColumnCount(4)
        self.productTable.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.productTable.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.productTable.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.productTable.setHorizontalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.productTable.setHorizontalHeaderItem(3, item)
        self.productTable.horizontalHeader().setDefaultSectionSize(149)
        self.productTable.verticalHeader().setVisible(False)
        self.label = QtGui.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(30, 40, 261, 21))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.label.setFont(font)
        self.label.setObjectName(_fromUtf8("label"))
        self.label_3 = QtGui.QLabel(Dialog)
        self.label_3.setGeometry(QtCore.QRect(360, 40, 81, 17))
        font = QtGui.QFont()
        font.setPointSize(13)
        self.label_3.setFont(font)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.new_product_btn = QtGui.QPushButton(Dialog)
        self.new_product_btn.setGeometry(QtCore.QRect(500, 400, 131, 27))
        self.new_product_btn.setFocusPolicy(QtCore.Qt.NoFocus)
        self.new_product_btn.setObjectName(_fromUtf8("new_product_btn"))
        self.lineEdit_search = QtGui.QLineEdit(Dialog)
        self.lineEdit_search.setGeometry(QtCore.QRect(450, 40, 161, 27))
        self.lineEdit_search.setReadOnly(False)
        self.lineEdit_search.setObjectName(_fromUtf8("lineEdit_search"))
        self.label_totalP = QtGui.QLabel(Dialog)
        self.label_totalP.setGeometry(QtCore.QRect(40, 410, 66, 17))
        self.label_totalP.setObjectName(_fromUtf8("label_totalP"))
        self.delete_product_btn = QtGui.QPushButton(Dialog)
        self.delete_product_btn.setEnabled(False)
        self.delete_product_btn.setGeometry(QtCore.QRect(360, 400, 131, 27))
        self.delete_product_btn.setFocusPolicy(QtCore.Qt.NoFocus)
        self.delete_product_btn.setObjectName(_fromUtf8("delete_product_btn"))

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Dialog", None))
        item = self.productTable.horizontalHeaderItem(0)
        item.setText(_translate("Dialog", "Nombre", None))
        item = self.productTable.horizontalHeaderItem(1)
        item.setText(_translate("Dialog", "Código", None))
        item = self.productTable.horizontalHeaderItem(2)
        item.setText(_translate("Dialog", "Precio", None))
        item = self.productTable.horizontalHeaderItem(3)
        item.setText(_translate("Dialog", "Cantidad", None))
        self.label.setText(_translate("Dialog", "Productos", None))
        self.label_3.setText(_translate("Dialog", "Búsqueda", None))
        self.new_product_btn.setText(_translate("Dialog", "Nuevo Producto", None))
        self.label_totalP.setText(_translate("Dialog", "Total ", None))
        self.delete_product_btn.setText(_translate("Dialog", "Eliminar Producto", None))

