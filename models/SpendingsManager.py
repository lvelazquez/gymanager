#!/usr/bin/python
#coding=utf-8
 
from PyQt4 import QtGui, QtCore
from Utils import *
from datetime import datetime,date
import Constants
import sys



try:
	_fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
	def _fromUtf8(s):
		return s

 
# Import the interface class
import SpendingsManagerUI


 
class SpendingsManager(QtGui.QDialog, SpendingsManagerUI.Ui_Dialog):
	""" The second parent must be 'Ui_<obj. name of main widget class>'.
		If confusing, simply open up ImageViewer.py and get the class
		name used. I'd named mine as mainWindow, hence Ui_mainWindow. """

 
	def __init__(self, params=None,parent=None):
		super(SpendingsManager,self).__init__(parent)
		# This is because Python does not automatically
		# call the parent's constructor.
		self.setupUi(self)


		self.spendings_1 = None
		self.spendings_2 = None
		#Settings click handlers

		#self.productTable.clicked.connect(self.select_product)
		#self.lineEdit_search.returnPressed.connect(self.search_product)
		params = {}
		params['user'] = Constants.MANAGER
		self.is_manager = True if params['user'] == Constants.MANAGER else False

		#if self.is_manager:
		#    self.new_product_btn.clicked.connect(self.new_product)
		#else:
		#    self.new_product_btn.hide()

		#set client list
		self.set_spendings_table()
		self.set_hanlders()
		#from controllers import SpendingController
		#SpendingController.total_price(ss)
		#SpendingController.today_spendings()

		# Pass this "self" for building widgets and
		# keeping a reference.

 
	def main(self):
		self.show()

	def selected_spending(self,itemclicked,table):

		# #selected row index
		row = itemclicked.row()
		max_column = table.columnCount() - 1
		range_selected = QtGui.QTableWidgetSelectionRange(row,0,row,max_column)
		table.setRangeSelected(range_selected,True)

		spending_code = str(table.item(row,0).text())
		return spending_code

	def view_selected_spending(self,itemclicked):

		from controllers import SpendingController
		
		sales_code = self.selected_spending(itemclicked)
		index = map(lambda x:x.id,self.sales).index(int(sales_code))
		selected_spending = self.sales[index]
		#selected_product = ProductController.search_product(code=product_code)

		#self.open_spendingform_window(selected_spending)

	#set initial client table configuration
	def set_spendings_table(self):

		#render client list
		self.render_spendings_table()
		today = date.today()
		self.dateEdit_search_month_1.setDate(QtCore.QDate(today.year, today.month,today.day))
		self.dateEdit_search_year_1.setDate(QtCore.QDate(today.year, today.month,today.day))


		#set no editable
		#if not self.is_manager:
		self.spendings_table_1.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
		self.spendings_table_2.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)

	def set_hanlders(self):
		#self.lineEdit_search.returnPressed.connect(self.search_spending)
		self.dateEdit_search_month_1.\
		dateChanged.connect(self.search_by_date)
		self.dateEdit_search_year_1.\
		dateChanged.connect(self.search_by_date)
		#self.spendings_table_1.doubleClicked.connect(self.view_selected_spending)
		cashier1_table = self.spendings_table_1
		cashier2_table = self.spendings_table_2
		self.spendings_table_1.clicked.connect(lambda item,table=cashier1_table:\
												  self.selected_spending(item,table)
												 )
		self.spendings_table_2.clicked.connect(lambda item,table=cashier2_table:\
												  self.selected_spending(item,table)
												 )


	#render complete client list
	def render_spendings_table(self):
		from controllers import SpendingController
		today = date.today()
		cashier_1 = 1
		cashier_2 = 2
		spendings_1 = SpendingController.search_by_month_year(today.month,today.year,cashier_1) if not self.spendings_1 else self.spendings_1
		self.spendings_1 = spendings_1

		atts_to_render = SpendingController.attributes_to_render()
		Utils.render_items_list(self.spendings_table_1,spendings_1,atts_to_render)
		total = sum(map(lambda x:x.total,spendings_1))
		self.total_spendings_1.display(total)


		spendings_2 = SpendingController.search_by_month_year(today.month,today.year,cashier_2) if not self.spendings_2 else self.spendings_2
		self.spendings_2 = spendings_2

		atts_to_render = SpendingController.attributes_to_render()
		Utils.render_items_list(self.spendings_table_2,spendings_2,atts_to_render)
		total = sum(map(lambda x:x.total,spendings_2))
		self.total_spendings_2.display(total)


	def render_matched_sales(self,sales_items):
		Utils.render_matched_items(self.sales_table,sales_items)


	#Handler for new_client_btn click
	def new_product(self):
		result = self.open_product_window()
		self.render_product_list()

	def search_spending(self):

		search_string = self.lineEdit_search.text()
		#if search string is empty, render complete client table
		self.render_spendings_table()            
		if not search_string:
			return

		matched_spendings = self.sales_table.findItems(search_string, QtCore.Qt.MatchContains)

		if matched_spendings:
			self.render_matched_sales(matched_spendings)
		
	def search_by_date(self):
		from controllers import SpendingController
		month  = datetime.strptime(str(self.dateEdit_search_month_1.text()) , '%m').month
		year   = datetime.strptime(str(self.dateEdit_search_year_1.text()) , "%Y").year
		cashier_1 = 1
		cashier_2 = 2

		
		spendings = SpendingController.search_by_month_year(month,year,cashier_1)
		atts_to_render = SpendingController.attributes_to_render()
		Utils.render_items_list(self.spendings_table_1,spendings,atts_to_render)
		total = sum(map(lambda x:x.total,spendings))
		self.total_spendings.display(total)

		spendings = SpendingController.search_by_month_year(month,year,cashier_2)
		atts_to_render = SpendingController.attributes_to_render()
		Utils.render_items_list(self.spendings_table_2,spendings,atts_to_render)
		total = sum(map(lambda x:x.total,spendings))
		self.total_spendings.display(total)

