#!/usr/bin/python
#coding=utf-8
 
from PyQt4 import QtGui, QtCore
from Utils import *
import Constants
import sys



try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

 
# Import the interface class
import ProductsUI


 
class Products(QtGui.QDialog, ProductsUI.Ui_Dialog):
    """ The second parent must be 'Ui_<obj. name of main widget class>'.
        If confusing, simply open up ImageViewer.py and get the class
        name used. I'd named mine as mainWindow, hence Ui_mainWindow. """

 
    def __init__(self, params,parent=None):
        super(Products,self).__init__(parent)
        # This is because Python does not automatically
        # call the parent's constructor.
        self.setupUi(self)


        self.products = None
        #Settings click handlers

        self.productTable.clicked.connect(self.select_product)
        self.productTable.doubleClicked.connect(self.view_selected_product)
        self.lineEdit_search.returnPressed.connect(self.search_product)
        self.delete_product_btn.clicked.connect(self.delete_product)

        
        self.is_manager = True if params['user'] == Constants.MANAGER else False

        if self.is_manager:
            self.new_product_btn.clicked.connect(self.new_product)
        else:
            self.new_product_btn.hide()

        #set client list
        self.set_product_table()


        # Pass this "self" for building widgets and
        # keeping a reference.

 
    def main(self):
        self.show()

    def select_product(self,itemclicked):

        #selected row index
        row = itemclicked.row()
        max_column = self.productTable.columnCount() - 1
        range_selected = QtGui.QTableWidgetSelectionRange(row,0,row,max_column)
        self.productTable.setRangeSelected(range_selected,True)

        product_code = str(self.productTable.item(row,1).text())
        self.current_product = itemclicked

        self.delete_product_btn.setEnabled(True)
        return product_code

    def view_selected_product(self,itemclicked):

        from controllers import ProductController
        
        product_code = self.select_product(itemclicked)
        selected_product = ProductController.search_product(code=product_code)

        self.open_product_window(selected_product)

    #set initial client table configuration
    def set_product_table(self):

        #render client list
        self.render_product_list()

        #set no editable
        if not self.is_manager:
            self.delete_product_btn.hide()
            self.productTable.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)

    #render complete client list
    def render_product_list(self):
        from controllers import ProductController

        products = ProductController.all_products() if not self.products else self.products
        atts_to_render = ProductController.attributes_to_render()
        Utils.render_items_list(self.productTable,products,atts_to_render)

        self.label_totalP.setText('Total ' + str(len(products)))

    def render_matched_products(self,product_items):

        Utils.render_matched_items(self.productTable,product_items)


    #Handler for new_client_btn click
    def new_product(self):
        result = self.open_product_window()
        self.render_product_list()

    def delete_product(self):

        row = self.current_product.row()
        product_id = int(self.productTable.item(row,1).text())
        from controllers import ProductController
        deleted = ProductController.delete_product(product_id)
        if not deleted:
            QtGui.QMessageBox.about(self,_fromUtf8("Notificación"),_fromUtf8("Producto no Eliminado"))
        else:
            self.productTable.removeRow(row)
            QtGui.QMessageBox.about(self,_fromUtf8("Notificación"),_fromUtf8("Producto Eliminado"))



    def search_product(self):

        search_string = self.lineEdit_search.text()
        #if search string is empty, render complete client table
        self.render_product_list()            
        if not search_string:
            return

        matched_products = self.productTable.findItems(search_string, QtCore.Qt.MatchContains)
        print "####################################"
        for c in matched_products:
            print c.text()
        print "####################################"

        if matched_products:
            self.render_matched_products(matched_products)
        

    def open_product_window(self,product_info=None):
        from ProductForm import ProductForm
        #app = QtGui.QApplication(sys.argv)
        window_params = {}
        window_params["is_manager"]   = self.is_manager
        window_params["product_info"] = product_info
        ProductForm = ProductForm(window_params)
        if ProductForm.exec_() == Constants.OBJECT_CREATED:
            self.render_product_list()






 
if __name__=='__main__':
    app = QtGui.QApplication(sys.argv)
    Product = Products()
    Product.main()
    app.exec_()
    # This shows the interface we just created. No logic has been added, yet.