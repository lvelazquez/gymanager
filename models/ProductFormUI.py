# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ProductFormUI.ui'
#
# Created: Wed Apr 24 00:13:54 2013
#      by: PyQt4 UI code generator 4.10
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(361, 298)
        self.label = QtGui.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(90, 20, 201, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.formLayoutWidget = QtGui.QWidget(Dialog)
        self.formLayoutWidget.setGeometry(QtCore.QRect(40, 50, 281, 161))
        self.formLayoutWidget.setObjectName(_fromUtf8("formLayoutWidget"))
        self.formLayout = QtGui.QFormLayout(self.formLayoutWidget)
        self.formLayout.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setMargin(0)
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.label_name = QtGui.QLabel(self.formLayoutWidget)
        self.label_name.setObjectName(_fromUtf8("label_name"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label_name)
        self.lineEdit_name = QtGui.QLineEdit(self.formLayoutWidget)
        self.lineEdit_name.setObjectName(_fromUtf8("lineEdit_name"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEdit_name)
        self.label_amount = QtGui.QLabel(self.formLayoutWidget)
        self.label_amount.setObjectName(_fromUtf8("label_amount"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.label_amount)
        self.lineEdit_amount = QtGui.QLineEdit(self.formLayoutWidget)
        self.lineEdit_amount.setObjectName(_fromUtf8("lineEdit_amount"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.lineEdit_amount)
        self.label_price = QtGui.QLabel(self.formLayoutWidget)
        self.label_price.setObjectName(_fromUtf8("label_price"))
        self.formLayout.setWidget(3, QtGui.QFormLayout.LabelRole, self.label_price)
        self.lineEdit_price = QtGui.QLineEdit(self.formLayoutWidget)
        self.lineEdit_price.setObjectName(_fromUtf8("lineEdit_price"))
        self.formLayout.setWidget(3, QtGui.QFormLayout.FieldRole, self.lineEdit_price)
        self.lineEdit_code = QtGui.QLineEdit(self.formLayoutWidget)
        self.lineEdit_code.setEnabled(True)
        self.lineEdit_code.setObjectName(_fromUtf8("lineEdit_code"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEdit_code)
        self.label_code = QtGui.QLabel(self.formLayoutWidget)
        self.label_code.setObjectName(_fromUtf8("label_code"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_code)
        self.comboBox_category = QtGui.QComboBox(self.formLayoutWidget)
        self.comboBox_category.setObjectName(_fromUtf8("comboBox_category"))
        self.formLayout.setWidget(4, QtGui.QFormLayout.FieldRole, self.comboBox_category)
        self.label_2 = QtGui.QLabel(self.formLayoutWidget)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.formLayout.setWidget(4, QtGui.QFormLayout.LabelRole, self.label_2)
        self.buttonBox = QtGui.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(150, 240, 176, 27))
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))

        self.retranslateUi(Dialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), Dialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Dialog", None))
        self.label.setText(_translate("Dialog", "Registrar Nuevo Producto", None))
        self.label_name.setText(_translate("Dialog", "Nombre", None))
        self.label_amount.setText(_translate("Dialog", "Cantidad", None))
        self.label_price.setText(_translate("Dialog", "Precio", None))
        self.label_code.setText(_translate("Dialog", "Código", None))
        self.label_2.setText(_translate("Dialog", "Categoria", None))

