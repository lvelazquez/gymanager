#coding=utf-8
from models import OwnerLocker,Locker, Client, Product, Spending,Settings,Sales,BillingMonthlyCode
from datetime import datetime,date,time
import hashlib



class ClientController(object):
	"""docstring for ClientController"""
	def __init__(self):
		super(ClientController, self).__init__()

	@classmethod
	def new_client(cls,client_params):
		
		#levi = Client('Levi','19737450','direccion','4','4','2007-08-05','2007-12-05','fotolevi','huellalevi')
		new_client = Client.add_client(client_params)
		return new_client
	
	@classmethod
	def search_client(cls,ci=None,name=None):
		if ci:
			return	Client.get_by_ci(ci)
		elif name:
			return	Client.get_by_name(name)

	@classmethod
	def all_clients(cls):
		clients = Client.all()
		return clients

	@classmethod
	def attributes_to_render(cls):
		return ['name','ci','signout_date','tel_mobil']

	@classmethod
	def billing_history(cls,client_ci):
		sales = SalesController.client_history(client_ci)
		return sales

	@classmethod
	def sales_history(cls,client):
		#client = ClientController.search_client(ci=19737450)
		_sales = Sales.get_by_client_lastYear(client.ci)
		sales  = [s.to_dict() for s in _sales]
		_products_sold=[]
		for sale in sales:
			_products_sold += [{'date':sale['date'],'id':sale['id'],'product_category':p['category'],'product_name':p['name'],'price':p['price'],\
							   'payments':sale['payments']} for p in sale['products_sold']]
		return _products_sold

	@classmethod
	def code_is_valid(cls,code,type_for):
		billing_code = BillingMonthlyCode.get_by_code(code)
		if not billing_code or not(type_for == billing_code.type):
			raise Exception("Codigo no valido")
		return True


	@classmethod
	def register_code(cls,client_ci,code):
		client = cls.search_client(ci=client_ci)
		billing_code = BillingMonthlyCode.get_by_code(code)
		if not billing_code:
			raise Exception("Codigo no valido")
		
		new_sod = billing_code.signout_date
		client.set_new_sod(new_sod)
		client.set_membership('normal')
		registered_code = billing_code.use_code(client)
		if registered_code:
			return True
		





class SettingsController(object):
	"""docstring for ClientController"""
	def __init__(self):
		super(SettingsController, self).__init__()

	@classmethod
	def get_iva(self):
		return Settings.get_iva();

	@classmethod
	def change_iva(self,new_iva):
		Settings.edit_iva(new_iva)

class ProductController(object):
	"""docstring for ClientController"""
	def __init__(self):
		super(ProductController, self).__init__()


	@classmethod
	def new_product(self,product_params):
		new_product = Product.add(product_params)
		return new_product

	@classmethod
	def delete_product(self,product_id):
		try:
			Product.delete(product_id)
			return True
		except Exception,e:
			return False


	@classmethod
	def all_products(self):
		return Product.all();

	@classmethod
	def search_product(cls,code):
		return	Product.get_by_code(code)

	@classmethod
	def get_products_category(cls,category):
		products = Product.get_by_category(category)
		return products

	@classmethod
	def products_report(cls,month,year,cashier_number):
		products = Product.products_report(month,year,cashier_number)
		replace = lambda x: 0 if not x else x		
		_final_p = []
		for p in products:
			_p = {}
			_p['product'] = replace(p[0])
			_p['amount']  = replace(p[1])
			_p['total']   = replace(p[2])
			_final_p.append(_p) 

		return _final_p

	@classmethod
	def attributes_to_render(cls):
		return ['name','id','price','amount']

	@classmethod
	def product_category(cls):
		return ['Mensualidad','Inscripcion','Planes',
				'Reinscripcion','Mantenimiento','Bebida',
				'Locker','Reservacion']



class SpendingController(object):
	"""docstring for ClientController"""
	def __init__(self):
		super(SpendingController, self).__init__()


	@classmethod
	def new_spending(self,spending_params,payments):
		new_spending = Spending.add(spending_params,payments)
		return new_spending

	@classmethod
	def all_spending(self):
		return Spending.all()

	@classmethod
	def today_spendings(self,cashier_number):
		today = date.today()
		spendings = Spending.get_by_date(today)
		return spendings

	@classmethod
	def search_by_date(self,date,cashier_number):
		spendings = Spending.get_by_date(date,cashier_number)
		return spendings

	@classmethod
	def search_by_month_year(cls,month,year,cashier_number):
		spendings = Spending.get_by_month_year(month,year,cashier_number)
		return spendings


	@classmethod
	def search_spending(cls,code):
		return	Spending.get_by_code(code)

	@classmethod
	def total_price(cls,payments):
		payments = spending.payments
		total = sum(map(lambda x:x.amount,payments))
		return total


	@classmethod
	def attributes_to_render(cls):
		return ['id','beneficiary','total','created_date']




class SalesController(object):
	"""docstring for ClientController"""
	def __init__(self):
		super(SalesController, self).__init__()


	@classmethod
	def register_sale(self,sales_params,payments):
		new_sod  = sales_params['new_sod_date']
		client   = sales_params['client']
		products = sales_params['products']
		payments = sales_params['payments']
		enrollments_number = sales_params['enrollments_numbers']
		monthly_payment_number = sales_params['monthly_payment_number']
		sales    = {}
		sales['cashier_number'] = sales_params['cashier_number']
		membership_type = sales_params['membership_type']

		#if there are many enrollments
		if enrollments_number>1:
			
			#_new_sod = datetime.date(new_sod.year, new_sod.month, 30)
			_new_sod = datetime.strptime(str(new_sod.year)+str(new_sod.month)+'30', "%Y%m%d").date()
			code = hashlib.md5(str(datetime.now())+client.name).hexdigest()[::-1][:10]
			code_params = {}
			code_params['code'] = code
			code_params['signout_date'] = _new_sod
			code_params['valid_for'] = enrollments_number
			code_params['type'] = 'enrollment'
			billing_code = BillingMonthlyCode.add(code_params)

		if monthly_payment_number>1:
			_new_sod = datetime.strptime(str(new_sod.year)+str(new_sod.month)+'30', "%Y%m%d").date()
			code = hashlib.md5(str(datetime.now())+client.name).hexdigest()[::-1][:10]
			code_params = {}
			code_params['code'] = code
			code_params['signout_date'] = _new_sod
			code_params['valid_for'] = monthly_payment_number
			code_params['type'] = 'monthly_payment'
			billing_code = BillingMonthlyCode.add(code_params)
			print billing_code.valid_for,billing_code.type,billing_code.signout_date

		if membership_type:
			current_membership = client.membership
			if current_membership == 'mensualidad sin inscripcion':
				membership_type = 'normal'
		
		client.set_membership(membership_type)		

		client.set_new_sod(new_sod)
		
		new_sale = Sales.add(sales,payments,products,client)
		if not new_sale:
			return False

		return new_sale


	@classmethod
	def delete_sale(self,code):
		try:
			Sales.delete(code)
			return True
		except Exception,e:
			return False

	@classmethod
	def all_sales(self):
		return Sales.all()

	@classmethod
	def today_sales(self):
		today = date.today()
		sales = Sales.get_by_date(today)
		return sales

	@classmethod
	def search_by_date(cls,date,cashier_number):
		sales = Sales.get_by_date(date,cashier_number)
		products_sold = cls.products_sold(sales)
		return products_sold

	@classmethod
	def search_by_month_year(cls,month,year,cashier_number):
		sales = Sales.get_by_month_year(month,year,cashier_number)

		_final_sales = []
		for index in range(1,32):
			_s = {}
			_s['total']   = 0
			_s['amount']  = 0
			_s['day']     = index
			_final_sales.append(_s)

		for s in sales:
			_day = s[2]
			_final_sales[_day-1]['amount']  = s[0]
			_final_sales[_day-1]['total']   = s[1]

		return _final_sales

	@classmethod
	def search_spending(cls,code):
		return	Spending.get_by_code(code)

	@classmethod
	def total_price(cls,payments):
		payments = spending.payments
		total = sum(map(lambda x:x.amount,payments))
		return total

	@classmethod
	def client_history(cls,client):
		sales = Sales.get_by_client(client)
		return sales

	@classmethod
	def last_year_history(cls,client_ci):
		return Sales.get_by_client_lastYear(client_ci)

	@classmethod
	def attributes_to_render(cls):
		return ['id','beneficiary','total','created_date']

	@classmethod
	def product_category(cls):
		return ['varios']

	@classmethod
	def products_sold(cls,sales):
		_sales  = [s.to_dict() for s in sales]
		_products_sold=[]
		for sale in _sales:
			_products_sold += [{'id':sale['id'],'date':sale['date'],'product_category':p['category'],\
			                    'client':sale['client_name'],'product_name':p['name'],'price':p['price'],\
							    'payments':sale['payments']} for p in sale['products_sold']]

		return _products_sold


#ClientController.new_client({'hola':'hola'})