#!/usr/bin/python
#coding=utf-8
 
from PyQt4 import QtGui, QtCore
from Utils import *
import Constants

import sys



try:
	_fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
	def _fromUtf8(s):
		return s

 
# Import the interface class
import ClientHistoryUI


 
class ClientHistory(QtGui.QDialog, ClientHistoryUI.Ui_Dialog):
	""" The second parent must be 'Ui_<obj. name of main widget class>'.
		If confusing, simply open up ImageViewer.py and get the class
		name used. I'd named mine as mainWindow, hence Ui_mainWindow. """

 
	def __init__(self, window_params, parent=None):
		super(ClientHistory,self).__init__(parent)
		# This is because Python does not automatically
		# call the parent's constructor.
		self.setupUi(self)

		self.is_manager = True

		self.client = window_params['client']

		#self.set_handlers()

		#self.clients = None
				
		#set client list
		self.set_bill_table()

		# Pass this "self" for building widgets and
		# keeping a reference.
 
	def main(self):
		self.show()

#/************************RENDER METHOS********************/#

	def render_bill_history(self):

		from controllers import ClientController

		sales_render = ClientController.sales_history(self.client)
		items = []
		for s in sales_render:
			print s
		for sale in sales_render:
			items.append(Utils.set_att(sale))
		#print sales_render
		atts_to_render = ['date','id','product_category','product_name','price']
		Utils.render_items_list(self.billhistory_table,items,atts_to_render)


#/************************SETTINGS METHOS********************/#

	#set initial client table configuration
	def set_bill_table(self):

		#render client list
		self.render_bill_history()

		#set no editable
		if not self.is_manager:
			self.billhistory_table.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)

