# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ProductsReportUI.ui'
#
# Created: Fri Jun 21 00:13:43 2013
#      by: PyQt4 UI code generator 4.10
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(785, 634)
        self.frame = QtGui.QFrame(Dialog)
        self.frame.setGeometry(QtCore.QRect(10, 40, 391, 571))
        self.frame.setStyleSheet(_fromUtf8("background-color: rgb(0, 170, 255);\n"
""))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.sales_table_cashier1 = QtGui.QTableWidget(self.frame)
        self.sales_table_cashier1.setGeometry(QtCore.QRect(50, 50, 299, 471))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sales_table_cashier1.sizePolicy().hasHeightForWidth())
        self.sales_table_cashier1.setSizePolicy(sizePolicy)
        self.sales_table_cashier1.setStyleSheet(_fromUtf8("background-color: rgb(255, 255, 255);"))
        self.sales_table_cashier1.setObjectName(_fromUtf8("sales_table_cashier1"))
        self.sales_table_cashier1.setColumnCount(3)
        self.sales_table_cashier1.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.sales_table_cashier1.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.sales_table_cashier1.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.sales_table_cashier1.setHorizontalHeaderItem(2, item)
        self.sales_table_cashier1.horizontalHeader().setDefaultSectionSize(99)
        self.sales_table_cashier1.horizontalHeader().setMinimumSectionSize(31)
        self.sales_table_cashier1.verticalHeader().setVisible(False)
        self.horizontalLayoutWidget_2 = QtGui.QWidget(self.frame)
        self.horizontalLayoutWidget_2.setGeometry(QtCore.QRect(190, 520, 160, 41))
        self.horizontalLayoutWidget_2.setObjectName(_fromUtf8("horizontalLayoutWidget_2"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.horizontalLayoutWidget_2)
        self.horizontalLayout_2.setMargin(0)
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label_3 = QtGui.QLabel(self.horizontalLayoutWidget_2)
        self.label_3.setStyleSheet(_fromUtf8("color: rgb(255, 255, 255);"))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_2.addWidget(self.label_3)
        self.total_sales = QtGui.QLCDNumber(self.horizontalLayoutWidget_2)
        self.total_sales.setObjectName(_fromUtf8("total_sales"))
        self.horizontalLayout_2.addWidget(self.total_sales)
        self.horizontalLayoutWidget_3 = QtGui.QWidget(self.frame)
        self.horizontalLayoutWidget_3.setGeometry(QtCore.QRect(20, 10, 162, 31))
        self.horizontalLayoutWidget_3.setObjectName(_fromUtf8("horizontalLayoutWidget_3"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout(self.horizontalLayoutWidget_3)
        self.horizontalLayout_3.setMargin(0)
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.label_4 = QtGui.QLabel(self.horizontalLayoutWidget_3)
        self.label_4.setStyleSheet(_fromUtf8("background-color: rgb(0, 170, 255);\n"
"color: rgb(255, 255, 255);\n"
""))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.horizontalLayout_3.addWidget(self.label_4)
        self.dateEdit_search_month_1 = QtGui.QDateEdit(self.horizontalLayoutWidget_3)
        self.dateEdit_search_month_1.setStyleSheet(_fromUtf8("background-color: rgb(255, 255, 255);"))
        self.dateEdit_search_month_1.setDate(QtCore.QDate(2013, 1, 31))
        self.dateEdit_search_month_1.setCalendarPopup(False)
        self.dateEdit_search_month_1.setObjectName(_fromUtf8("dateEdit_search_month_1"))
        self.horizontalLayout_3.addWidget(self.dateEdit_search_month_1)
        self.horizontalLayoutWidget_6 = QtGui.QWidget(self.frame)
        self.horizontalLayoutWidget_6.setGeometry(QtCore.QRect(190, 10, 162, 31))
        self.horizontalLayoutWidget_6.setObjectName(_fromUtf8("horizontalLayoutWidget_6"))
        self.horizontalLayout_6 = QtGui.QHBoxLayout(self.horizontalLayoutWidget_6)
        self.horizontalLayout_6.setMargin(0)
        self.horizontalLayout_6.setObjectName(_fromUtf8("horizontalLayout_6"))
        self.label_7 = QtGui.QLabel(self.horizontalLayoutWidget_6)
        self.label_7.setStyleSheet(_fromUtf8("background-color: rgb(0, 170, 255);\n"
"color: rgb(255, 255, 255);\n"
""))
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.horizontalLayout_6.addWidget(self.label_7)
        self.dateEdit_search_year_1 = QtGui.QDateEdit(self.horizontalLayoutWidget_6)
        self.dateEdit_search_year_1.setStyleSheet(_fromUtf8("background-color: rgb(255, 255, 255);"))
        self.dateEdit_search_year_1.setDate(QtCore.QDate(2013, 3, 7))
        self.dateEdit_search_year_1.setObjectName(_fromUtf8("dateEdit_search_year_1"))
        self.horizontalLayout_6.addWidget(self.dateEdit_search_year_1)
        self.frame_2 = QtGui.QFrame(Dialog)
        self.frame_2.setGeometry(QtCore.QRect(410, 40, 341, 571))
        self.frame_2.setStyleSheet(_fromUtf8("background-color: rgb(195, 255, 64);"))
        self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))
        self.sales_table_cashier2 = QtGui.QTableWidget(self.frame_2)
        self.sales_table_cashier2.setGeometry(QtCore.QRect(20, 50, 299, 471))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sales_table_cashier2.sizePolicy().hasHeightForWidth())
        self.sales_table_cashier2.setSizePolicy(sizePolicy)
        self.sales_table_cashier2.setStyleSheet(_fromUtf8("background-color: rgb(255, 255, 255);"))
        self.sales_table_cashier2.setObjectName(_fromUtf8("sales_table_cashier2"))
        self.sales_table_cashier2.setColumnCount(3)
        self.sales_table_cashier2.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.sales_table_cashier2.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.sales_table_cashier2.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.sales_table_cashier2.setHorizontalHeaderItem(2, item)
        self.sales_table_cashier2.horizontalHeader().setDefaultSectionSize(99)
        self.sales_table_cashier2.horizontalHeader().setMinimumSectionSize(31)
        self.sales_table_cashier2.verticalHeader().setVisible(False)
        self.horizontalLayoutWidget_4 = QtGui.QWidget(self.frame_2)
        self.horizontalLayoutWidget_4.setGeometry(QtCore.QRect(160, 520, 160, 41))
        self.horizontalLayoutWidget_4.setObjectName(_fromUtf8("horizontalLayoutWidget_4"))
        self.horizontalLayout_4 = QtGui.QHBoxLayout(self.horizontalLayoutWidget_4)
        self.horizontalLayout_4.setMargin(0)
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.label_5 = QtGui.QLabel(self.horizontalLayoutWidget_4)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.horizontalLayout_4.addWidget(self.label_5)
        self.total_sales_2 = QtGui.QLCDNumber(self.horizontalLayoutWidget_4)
        self.total_sales_2.setObjectName(_fromUtf8("total_sales_2"))
        self.horizontalLayout_4.addWidget(self.total_sales_2)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Dialog", None))
        item = self.sales_table_cashier1.horizontalHeaderItem(0)
        item.setText(_translate("Dialog", "Producto", None))
        item = self.sales_table_cashier1.horizontalHeaderItem(1)
        item.setText(_translate("Dialog", "Cantidad", None))
        item = self.sales_table_cashier1.horizontalHeaderItem(2)
        item.setText(_translate("Dialog", "Monto", None))
        self.label_3.setText(_translate("Dialog", "Total", None))
        self.label_4.setText(_translate("Dialog", "Mes", None))
        self.dateEdit_search_month_1.setDisplayFormat(_translate("Dialog", "M", None))
        self.label_7.setText(_translate("Dialog", "Año", None))
        self.dateEdit_search_year_1.setDisplayFormat(_translate("Dialog", "yyyy", None))
        item = self.sales_table_cashier2.horizontalHeaderItem(0)
        item.setText(_translate("Dialog", "Producto", None))
        item = self.sales_table_cashier2.horizontalHeaderItem(1)
        item.setText(_translate("Dialog", "Cantidad", None))
        item = self.sales_table_cashier2.horizontalHeaderItem(2)
        item.setText(_translate("Dialog", "Monto", None))
        self.label_5.setText(_translate("Dialog", "Total", None))

