#!/usr/bin/python
#coding=utf-8
 
from PyQt4 import QtGui, QtCore
from Utils import *
import Constants

import sys



try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

 
# Import the interface class
import ClientsUI


 
class Clients(QtGui.QDialog, ClientsUI.Ui_Dialog):
    """ The second parent must be 'Ui_<obj. name of main widget class>'.
        If confusing, simply open up ImageViewer.py and get the class
        name used. I'd named mine as mainWindow, hence Ui_mainWindow. """

 
    def __init__(self, params, parent=None):
        super(Clients,self).__init__(parent)
        # This is because Python does not automatically
        # call the parent's constructor.
        self.setupUi(self)

        self.is_manager = True if params['user'] == Constants.MANAGER else False

        self.set_handlers()

        self.clients = None
                
        #set client list
        self.set_client_table()

        # Pass this "self" for building widgets and
        # keeping a reference.
 
    def main(self):
        self.show()

    #Handler for new_client_btn click
    def new_client(self):
        result = self.open_client_window()
        self.render_client_list()

    def search_clients(self):

        search_string = self.lineEdit_search.text()
        #if search string is empty, render complete client table
        self.render_client_list()            
        if not search_string:
            return

        matched_clients = self.clientTable.findItems(search_string, QtCore.Qt.MatchContains)

        if matched_clients:
            self.render_matched_clients(matched_clients)
        
    def open_client_window(self,selected_client=None):
        from ClientForm import ClientForm
        #app = QtGui.QApplication(sys.argv)
        window_params = {}
        window_params["is_manager"]   = self.is_manager
        window_params["client_info"]  = selected_client
        if not selected_client:
            window_params['is_new_client'] = True
        else:
            window_params['is_new_client'] = False

        ClientForm = ClientForm(window_params)
        if ClientForm.exec_() == Constants.OBJECT_CREATED:
            self.render_client_list()

    def select_client(self,itemclicked):

        #selected row index
        row = itemclicked.row()
        max_column = self.clientTable.columnCount() - 1
        range_selected = QtGui.QTableWidgetSelectionRange(row,0,row,max_column)
        self.clientTable.setRangeSelected(range_selected,True)

        client_ci = str(self.clientTable.item(row,1).text())
        #from controllers import ClientController
        #sales = ClientController.billing_history(client_ci)
        #self.render_bill_history(sales)
        return client_ci

    def view_selected_client(self,itemclicked):

        from controllers import ClientController
        
        client_ci = self.select_client(itemclicked)
        selected_client = ClientController.search_client(ci=client_ci)
        self.open_client_window(selected_client)

#/************************RENDER METHOS********************/#

    #render complete client list
    def render_client_list(self):
        from controllers import ClientController

        clients = ClientController.all_clients() if not self.clients else self.clients
        atts_to_render = ClientController.attributes_to_render()

        Utils.render_items_list(self.clientTable,clients,atts_to_render)


    def render_matched_clients(self,client_items):

        Utils.render_matched_items(self.clientTable,client_items)

    def render_bill_history(self,items):
        atts_to_render = ['id','products','total']
        Utils.render_items_list(self.billhistory_table,items,atts_to_render)


#/************************SETTINGS METHOS********************/#

    def set_handlers(self):

        self.new_client_btn.clicked.connect(self.new_client)
        #Settings click handlers
        #   self.new_client_btn.clicked.connect(self.new_client)
        self.clientTable.clicked.connect(self.select_client)
        self.clientTable.doubleClicked.connect(self.view_selected_client)
        self.lineEdit_search.returnPressed.connect(self.search_clients)

    #set initial client table configuration
    def set_client_table(self):

        #render client list
        self.render_client_list()

        #set no editable
        if not self.is_manager:
            self.clientTable.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)


 
if __name__=='__main__':
    app = QtGui.QApplication(sys.argv)
    Clients = Clients()
    Clients.main()
    app.exec_()
    # This shows the interface we just created. No logic has been added, yet.