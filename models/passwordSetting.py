#!/usr/bin/python
#coding=utf-8
 
from PyQt4 import QtGui, QtCore

import sys
 
# Import the interface class
import passwordSettingUI


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

 
class PasswordSetting(QtGui.QDialog, passwordSettingUI.Ui_Dialog):
    """ The second parent must be 'Ui_<obj. name of main widget class>'.
        If confusing, simply open up ImageViewer.py and get the class
        name used. I'd named mine as mainWindow, hence Ui_mainWindow. """
 
    def __init__(self, window_params=None, parent=None):
        super(PasswordSetting, self).__init__(parent)
        # This is because Python does not automatically
        # call the parent's constructor.
        self.setupUi(self)
        # Pass this "self" for building widgets and
        # keeping a reference.

        self.is_manager = False
 
    def main(self):
        self.show()

    #Rewrite fuction accept
    def accept(self):
        password = 'gymanager'
        input_password = self.lineEdit_pw.text();
        if not password == input_password:
            QtGui.QMessageBox.about(self,_fromUtf8("Alerta"),_fromUtf8("Contraseña incorrecta"))
            return;


        self.open_settings_window()
        self.close()

    def open_settings_window(self):
        from Settings import Settings
        #app = QtGui.QApplication(sys.argv)
        Settings = Settings()
        Settings.exec_()


if __name__=='__main__':
    app = QtGui.QApplication(sys.argv)
    PasswordSetting = PasswordSetting()
    PasswordSetting.main()
    app.exec_()
