# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ClientsUI.ui'
#
# Created: Fri May 31 00:10:14 2013
#      by: PyQt4 UI code generator 4.10
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(699, 625)
        self.new_client_btn = QtGui.QPushButton(Dialog)
        self.new_client_btn.setGeometry(QtCore.QRect(550, 510, 98, 27))
        self.new_client_btn.setFocusPolicy(QtCore.Qt.NoFocus)
        self.new_client_btn.setObjectName(_fromUtf8("new_client_btn"))
        self.clientTable = QtGui.QTableWidget(Dialog)
        self.clientTable.setGeometry(QtCore.QRect(50, 110, 602, 381))
        self.clientTable.setEditTriggers(QtGui.QAbstractItemView.DoubleClicked)
        self.clientTable.setObjectName(_fromUtf8("clientTable"))
        self.clientTable.setColumnCount(4)
        self.clientTable.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.clientTable.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.clientTable.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.clientTable.setHorizontalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.clientTable.setHorizontalHeaderItem(3, item)
        self.clientTable.horizontalHeader().setDefaultSectionSize(150)
        self.clientTable.verticalHeader().setVisible(False)
        self.label = QtGui.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(50, 80, 261, 21))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.label.setFont(font)
        self.label.setObjectName(_fromUtf8("label"))
        self.line = QtGui.QFrame(Dialog)
        self.line.setGeometry(QtCore.QRect(50, 60, 601, 20))
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.lineEdit_search = QtGui.QLineEdit(Dialog)
        self.lineEdit_search.setGeometry(QtCore.QRect(470, 80, 161, 27))
        self.lineEdit_search.setReadOnly(False)
        self.lineEdit_search.setObjectName(_fromUtf8("lineEdit_search"))
        self.label_3 = QtGui.QLabel(Dialog)
        self.label_3.setGeometry(QtCore.QRect(380, 80, 81, 17))
        font = QtGui.QFont()
        font.setPointSize(13)
        self.label_3.setFont(font)
        self.label_3.setObjectName(_fromUtf8("label_3"))

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Dialog", None))
        self.new_client_btn.setText(_translate("Dialog", "Nuevo Cliente", None))
        item = self.clientTable.horizontalHeaderItem(0)
        item.setText(_translate("Dialog", "Nombre", None))
        item = self.clientTable.horizontalHeaderItem(1)
        item.setText(_translate("Dialog", "Cédula", None))
        item = self.clientTable.horizontalHeaderItem(2)
        item.setText(_translate("Dialog", "Fecha Corte", None))
        item = self.clientTable.horizontalHeaderItem(3)
        item.setText(_translate("Dialog", "Teléfono Móvil", None))
        self.label.setText(_translate("Dialog", "Clientes", None))
        self.label_3.setText(_translate("Dialog", "Búsqueda", None))

