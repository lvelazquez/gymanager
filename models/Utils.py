#!/usr/bin/python
#coding=utf-8
 
from PyQt4 import QtGui, QtCore

import sys



try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s




class Utils(object):
	"""docstring for Utils"""
	def __init__(self):
		super(Utils, self).__init__()



	@classmethod
	def render_items_list(self,table,instances,atts_to_render,vertical_header=None):
		row = 0
		num_row = len(instances)
		max_column = table.columnCount()
		table.setRowCount(num_row)

		for instance in instances:
			if vertical_header:
				exec("header = instance.%s" % vertical_header)
				new_row = QtGui.QTableWidgetItem(header)
			else:
				new_row = QtGui.QTableWidgetItem()

			table.setVerticalHeaderItem(row, new_row)
			current_column = 0

			for att in atts_to_render:
			    if current_column < max_column:
			        exec("att_to_render = instance.%s" % att)
			        item = QtGui.QTableWidgetItem(_fromUtf8(str(att_to_render)))
			        table.setItem(row, current_column, item)
			    current_column +=1
			row +=1
            

	@classmethod
	def render_matched_items(self,table,items):
		_max_column = table.columnCount()
		_row = 0
		_max_row = table.rowCount()
		row_to_render = []
		#renproductch client item
		for item in items:

		    row_number = item.row()
		    #row_to_render.append(row_number)
		    #new_row = QtGui.QTableWidgetItem()
		    #table.setVerticalHeaderItem(row_number, new_row)
		    colunm_to_render = []
		    for index in range(0,_max_column):
		        att  = table.item(row_number,index).text()
		        item = QtGui.QTableWidgetItem(_fromUtf8(att))
		        #table.setItem(_row, index, item)
		        colunm_to_render.append(item)

		    row_to_render.append(colunm_to_render)
		    #_row +=1

		table.clearContents()

		for row in row_to_render:
		    new_row = QtGui.QTableWidgetItem()
		    for index in range(0,_max_column):
		        item = row[index]
		        table.setItem(_row, index, item)
		    _row +=1 

	@classmethod
	def insert_row(self,table,row):
		_max_column = len(row)
		_max_row = table.rowCount()
		table.setRowCount(_max_row + 1)

		for index in range(0,_max_column):
			print row[index]
			item = QtGui.QTableWidgetItem(str(row[index]))
			table.setItem(_max_row,index,item)


################PAYMENT AUX METHODS #############

	#Extract payment info from a payment table
	@classmethod	
	def get_payments_table(self,table_payment):
		_max_row = table_payment.rowCount()
		_max_column = table_payment.columnCount() - 1 
		payments = []
		pay_att = ['amount','bank','confirmation_number']
		total_amount = 0
		for row in range(0,_max_row):
			payment = {}
			payment['type'] = str(table_payment.verticalHeaderItem(row).text())
			for column in range(0,_max_column):
				item = table_payment.item(row,column)
				if item:
					if column==0:
						value = int(item.text())
						total_amount += value
				value = str(item.text())
				payment[pay_att[column]] = value

			payments.append(payment)
		return payments


################################################AUX METHODS###############################
	@classmethod
	def set_att(self,dict_a):
		class AttrDict(dict):
		    def __init__(self, *args, **kwargs):
		        super(AttrDict, self).__init__(*args, **kwargs)
		        self.__dict__ = self

		A = AttrDict(dict_a)
		return A		


