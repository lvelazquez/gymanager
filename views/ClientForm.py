#!/usr/bin/python
 
from PyQt4 import QtGui, QtCore
from controllers import ClientController
import sys
 
# Import the interface class
import ClientFormUI
 
class ClientForm(QtGui.QDialog, ClientFormUI.Ui_Dialog):
    """ The second parent must be 'Ui_<obj. name of main widget class>'.
        If confusing, simply open up ImageViewer.py and get the class
        name used. I'd named mine as mainWindow, hence Ui_mainWindow. """
 
    def __init__(self, parent=None):
        super(ClientForm, self).__init__(parent)
        # This is because Python does not automatically
        # call the parent's constructor.
        self.setupUi(self)
        # Pass this "self" for building widgets and
        # keeping a reference.
 
    def main(self):
        self.show()

    #Rewrite fuction accept
    def accept(self):
        print "name",self.lineEdit_name
        print "usuario creado"
 
if __name__=='__main__':
    app = QtGui.QApplication(sys.argv)
    ClientForm = ClientForm()
    ClientForm.main()
    app.exec_()
    # This shows the interface we just created. No logic has been added, yet.