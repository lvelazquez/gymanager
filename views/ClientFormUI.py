# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ClientFormUI.ui'
#
# Created: Sun Apr 14 01:04:43 2013
#      by: PyQt4 UI code generator 4.10
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(517, 482)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Dialog.sizePolicy().hasHeightForWidth())
        Dialog.setSizePolicy(sizePolicy)
        self.buttonBox = QtGui.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(120, 430, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.Foto = QtGui.QLabel(Dialog)
        self.Foto.setGeometry(QtCore.QRect(20, 30, 121, 121))
        self.Foto.setMinimumSize(QtCore.QSize(121, 0))
        self.Foto.setAutoFillBackground(True)
        self.Foto.setObjectName(_fromUtf8("Foto"))
        self.formLayoutWidget = QtGui.QWidget(Dialog)
        self.formLayoutWidget.setGeometry(QtCore.QRect(200, 50, 281, 161))
        self.formLayoutWidget.setObjectName(_fromUtf8("formLayoutWidget"))
        self.formLayout = QtGui.QFormLayout(self.formLayoutWidget)
        self.formLayout.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setMargin(0)
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.label_name = QtGui.QLabel(self.formLayoutWidget)
        self.label_name.setObjectName(_fromUtf8("label_name"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label_name)
        self.lineEdit = QtGui.QLineEdit(self.formLayoutWidget)
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEdit)
        self.label_ci = QtGui.QLabel(self.formLayoutWidget)
        self.label_ci.setObjectName(_fromUtf8("label_ci"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_ci)
        self.lineEdit_2 = QtGui.QLineEdit(self.formLayoutWidget)
        self.lineEdit_2.setObjectName(_fromUtf8("lineEdit_2"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEdit_2)
        self.label_tlfm = QtGui.QLabel(self.formLayoutWidget)
        self.label_tlfm.setObjectName(_fromUtf8("label_tlfm"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.label_tlfm)
        self.lineEdit_5 = QtGui.QLineEdit(self.formLayoutWidget)
        self.lineEdit_5.setObjectName(_fromUtf8("lineEdit_5"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.lineEdit_5)
        self.lineEdit_6 = QtGui.QLineEdit(self.formLayoutWidget)
        self.lineEdit_6.setObjectName(_fromUtf8("lineEdit_6"))
        self.formLayout.setWidget(3, QtGui.QFormLayout.FieldRole, self.lineEdit_6)
        self.label_tlfh = QtGui.QLabel(self.formLayoutWidget)
        self.label_tlfh.setObjectName(_fromUtf8("label_tlfh"))
        self.formLayout.setWidget(3, QtGui.QFormLayout.LabelRole, self.label_tlfh)
        self.formLayoutWidget_2 = QtGui.QWidget(Dialog)
        self.formLayoutWidget_2.setGeometry(QtCore.QRect(10, 220, 491, 121))
        self.formLayoutWidget_2.setObjectName(_fromUtf8("formLayoutWidget_2"))
        self.formLayout_2 = QtGui.QFormLayout(self.formLayoutWidget_2)
        self.formLayout_2.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout_2.setMargin(0)
        self.formLayout_2.setObjectName(_fromUtf8("formLayout_2"))
        self.direccion = QtGui.QLabel(self.formLayoutWidget_2)
        self.direccion.setObjectName(_fromUtf8("direccion"))
        self.formLayout_2.setWidget(0, QtGui.QFormLayout.LabelRole, self.direccion)
        self.direccion_2 = QtGui.QLabel(self.formLayoutWidget_2)
        self.direccion_2.setObjectName(_fromUtf8("direccion_2"))
        self.formLayout_2.setWidget(1, QtGui.QFormLayout.LabelRole, self.direccion_2)
        self.dateEdit = QtGui.QDateEdit(self.formLayoutWidget_2)
        self.dateEdit.setMaximumDate(QtCore.QDate(7999, 12, 31))
        self.dateEdit.setCalendarPopup(True)
        self.dateEdit.setObjectName(_fromUtf8("dateEdit"))
        self.formLayout_2.setWidget(1, QtGui.QFormLayout.FieldRole, self.dateEdit)
        self.direccion_3 = QtGui.QLabel(self.formLayoutWidget_2)
        self.direccion_3.setObjectName(_fromUtf8("direccion_3"))
        self.formLayout_2.setWidget(2, QtGui.QFormLayout.LabelRole, self.direccion_3)
        self.dateEdit_2 = QtGui.QDateEdit(self.formLayoutWidget_2)
        self.dateEdit_2.setCalendarPopup(True)
        self.dateEdit_2.setObjectName(_fromUtf8("dateEdit_2"))
        self.formLayout_2.setWidget(2, QtGui.QFormLayout.FieldRole, self.dateEdit_2)
        self.lineEdit_3 = QtGui.QLineEdit(self.formLayoutWidget_2)
        self.lineEdit_3.setObjectName(_fromUtf8("lineEdit_3"))
        self.formLayout_2.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEdit_3)
        self.label = QtGui.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(260, 20, 161, 17))
        self.label.setObjectName(_fromUtf8("label"))

        self.retranslateUi(Dialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), Dialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Client", None))
        self.Foto.setText(_translate("Dialog", "           Foto", None))
        self.label_name.setText(_translate("Dialog", "Nombre", None))
        self.label_ci.setText(_translate("Dialog", "Cédula", None))
        self.label_tlfm.setText(_translate("Dialog", "T. Móvil", None))
        self.label_tlfh.setText(_translate("Dialog", "T. Hab", None))
        self.direccion.setText(_translate("Dialog", "Dirección", None))
        self.direccion_2.setText(_translate("Dialog", "F. Inscrip.", None))
        self.dateEdit.setDisplayFormat(_translate("Dialog", "M/d/yyyy", None))
        self.direccion_3.setText(_translate("Dialog", "F. Corte", None))
        self.label.setText(_translate("Dialog", "Registrar Nuevo Cliente", None))

